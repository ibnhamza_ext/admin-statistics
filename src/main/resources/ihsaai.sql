-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 18, 2019 at 03:59 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `performance_schema_old`
--

-- --------------------------------------------------------

--
-- Table structure for table `acte`
--

CREATE TABLE `acte` (
  `id` int(11) NOT NULL,
  `nomenclature_id` int(11) NOT NULL,
  `cotation` varchar(255) NOT NULL,
  `description` longtext,
  `cout` float DEFAULT NULL,
  `tarif` float DEFAULT NULL,
  `type_tube` int(11) DEFAULT NULL,
  `normes` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `id_reactif` int(11) DEFAULT NULL,
  `duree_estime` int(11) DEFAULT NULL,
  `raccourcis` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acte`
--

INSERT INTO `acte` (`id`, `nomenclature_id`, `cotation`, `description`, `cout`, `tarif`, `type_tube`, `normes`, `id_reactif`, `duree_estime`, `raccourcis`) VALUES
(1, 1, '', 'Consultation', 500, 400, NULL, '', NULL, 5, 1),
(2, 617, '', 'QSDQSD', 120, 150, 0, '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE `consultation` (
  `id` int(11) NOT NULL,
  `conduiteatenir` longtext CHARACTER SET utf8,
  `dateConsultation` datetime DEFAULT NULL,
  `diagRetenu` longtext CHARACTER SET utf8,
  `diagSuppose` longtext CHARACTER SET utf8,
  `examenClinique` longtext CHARACTER SET utf8,
  `examenDemande` longtext CHARACTER SET utf8,
  `interrogatoire` longtext CHARACTER SET utf8,
  `motif` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `resultatExamen` longtext CHARACTER SET utf8,
  `patient` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`id`, `conduiteatenir`, `dateConsultation`, `diagRetenu`, `diagSuppose`, `examenClinique`, `examenDemande`, `interrogatoire`, `motif`, `resultatExamen`, `patient`) VALUES
(1, '', '2019-01-30 16:50:47', '', '', '', '', '', '', '', 1),
(2, '', '2019-01-30 17:26:15', '', '', '', '', '', '', '', 1),
(3, '', '2019-02-04 12:03:08', '', '', '', '', '', '', '', 1),
(4, '', '2019-02-04 12:06:31', '', '', '', '', '', '', '', 2),
(5, '', '2019-02-04 12:08:00', '', '', '', '', '', '', '', 2),
(6, '', '2019-02-05 15:03:41', '', '', '', '', '', '', '', 1),
(7, '', '2019-02-06 09:42:21', '', '', '', '', '', '', '', 1),
(8, '', '2019-02-06 09:44:49', '', '', '', '', '', '', '', 1),
(9, '', '2019-02-06 10:35:40', '', '', '', '', '', '', '', 2),
(11, '', '2019-02-06 10:40:32', '', '', '', '', '', '', '', 2),
(12, '', '2019-02-06 11:04:52', '', '', '', '', '', '', '', 2),
(13, '', '2019-02-06 11:57:12', '', '', '', '', '', '', '', 2),
(14, '', '2019-02-06 12:38:06', '', '', '', '', '', '', '', 2),
(15, '', '2019-02-06 12:38:29', '', '', '', '', '', '', '', 2),
(16, '', '2019-02-06 12:39:02', '', '', '', '', '', '', '', 2),
(17, '', '2019-02-06 14:10:46', '', '', '', '', '', '', '', 2),
(18, '', '2019-02-06 14:58:15', '', '', '', '', '', '', '', 2),
(19, '', '2019-02-06 14:59:20', '', '', '', '', '', '', '', 2),
(20, '', '2019-02-06 15:01:29', '', '', '', '', '', '', '', 2),
(21, '', '2019-02-21 12:39:59', '', '', '', '', '', '', '', 1),
(22, '', '2019-02-25 15:35:11', '', 'ezaezaezaeaze | ', '', 'eazeaze	 | ', '{\"id\":41,\"code\":\"P2-01300\",\"val\":\"anamnèse et examen physique\",\"nomc_id\":3,\"cat_id\":1} | ', '', '', 1),
(23, '', '2019-02-25 15:38:28', '', '', '', '', '', '', '', 1),
(24, '', '2019-02-27 15:05:33', '', '', '', '', '', '', '', 1),
(25, '', '2019-02-28 10:44:06', '', '', '', '', '', '', '', 1),
(26, '', '2019-03-17 17:56:31', '', '', '', '', '', '', '', 1),
(27, '', '2019-03-18 16:03:52', '', '', '', '', '', '', '', 1),
(28, '', '2019-03-19 13:46:02', '', '', '', '', '', '', '', 1),
(29, '', '2019-03-19 17:19:31', '', '', '', '', '', '', '', 1),
(30, '', '2019-03-28 15:07:00', '', '', '', '', '', '', '', 1),
(31, '', '2019-03-28 15:10:34', '', '', '', '', '', '', '', 1),
(32, '', '2019-03-28 15:18:06', '', '', '', '', '', '', '', 1),
(33, '', '2019-03-29 17:15:25', '', '', '', '', '', '', '', 1),
(34, '', '2019-04-22 12:06:25', 'yersiniose | ', 'yersiniose | ', '', '', '', '', '', 7),
(35, '', '2019-04-22 12:39:05', 'DEFICIENCE MENTALE | ', 'DEFICIENCE MENTALE | ', 'Fécalurie | ', '', '', '', '', 7),
(36, '', '2019-04-22 13:43:44', '', '', '', '', '', '', '', 9),
(37, '', '2019-04-22 15:06:36', '', '', '', '', 'Dyspepsie | ', '', '', 10),
(38, '', '2019-04-22 15:12:22', '', '', '', '', 'Raideur (rhumatologie) | ', '', '', 10),
(39, '', '2019-04-22 15:17:14', '', '', '', '', 'Écholalie | ', '', '', 10),
(40, '', '2019-04-22 15:25:08', '', '', '', '', 'Écholalie | ', '', '', 10),
(41, '', '2019-04-22 15:26:20', '', '', '', '', 'Gynécomastie | ', '', '', 10),
(42, '', '2019-04-22 15:35:00', '', '', 'Macule | ', '', '', '', '', 10),
(43, '', '2019-04-22 15:35:59', '', '', '', '', 'Écholalie | ', '', '', 10),
(44, '', '2019-04-22 15:36:26', '', '', '', '', '', '', 'Ascite | ', 10),
(45, '', '2019-04-22 15:46:02', '', '', '', '', 'Flapping | ', '', '', 9),
(46, '', '2019-04-22 15:48:20', '', '', '', '', 'Écholalie | ', '', '', 9),
(47, '', '2019-04-22 15:51:42', '', '', '', '', '', '', 'Ascite | ', 9),
(48, '', '2019-04-22 15:51:58', '', '', '', '', '', '', 'Aplasie | ', 9),
(49, '', '2019-06-24 06:06:48', '', '', '', '', '', '', '', 11),
(50, '', '2019-06-24 06:14:28', '', '', '', '', '', '', '', 11),
(51, '', '2019-06-24 06:16:33', '', '', 'Tension artérielle | ', '', 'trouble de la marche | Faux besoins (ténesme ) | Diarrhée | ', '', '', 11),
(52, '', '2019-07-07 07:06:48', '', '', 'Érythrose | ', '', '', '', '', 1),
(53, '', '2019-07-07 07:51:01', '', '', '', '', 'PYROSIS | ', '', '', 11),
(54, '', '2019-07-23 12:34:06', '', '', '', '', '', '', '', 1),
(55, '', '2019-07-23 14:58:47', '', '', '', '', '', '', '', 12),
(56, '', '2019-08-04 12:49:07', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_operation`
--

CREATE TABLE `detail_operation` (
  `id` int(11) NOT NULL,
  `operation_id` int(11) DEFAULT NULL,
  `acte_id` int(11) DEFAULT NULL,
  `medecin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_operation`
--

INSERT INTO `detail_operation` (`id`, `operation_id`, `acte_id`, `medecin_id`) VALUES
(1, 6, 1, NULL),
(2, 6, 2, NULL),
(3, 7, 2, NULL),
(4, 8, 1, NULL),
(5, 9, 1, NULL),
(6, 10, 1, NULL),
(7, 11, 1, NULL),
(8, 11, 2, NULL),
(9, 12, 1, NULL),
(10, 13, 2, NULL),
(11, 14, 2, NULL),
(12, 15, 1, NULL),
(13, 16, 1, NULL),
(14, 17, 1, NULL),
(15, 18, 2, NULL),
(16, 19, 2, NULL),
(17, 20, 1, NULL),
(18, 21, 1, NULL),
(19, 22, 1, NULL),
(20, 23, 2, NULL),
(21, 24, 2, NULL),
(22, 25, 2, NULL),
(23, 26, 1, NULL),
(24, 27, 1, NULL),
(25, 28, 1, NULL),
(26, 29, 1, NULL),
(27, 30, 2, NULL),
(28, 31, 1, NULL),
(29, 32, 1, NULL),
(30, 33, 1, NULL),
(31, 29, 1, NULL),
(32, 29, 1, NULL),
(33, 8, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fonctionnaire`
--

CREATE TABLE `fonctionnaire` (
  `id` int(11) NOT NULL,
  `compte` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mot_de_passe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `adresse` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `specialite` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `affectation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chr` int(11) DEFAULT NULL,
  `classification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conges` int(11) DEFAULT NULL,
  `date_entree` date DEFAULT NULL,
  `horaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `i_acte` int(11) DEFAULT NULL,
  `irg` float DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode_recrutement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n_compte` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n_ss` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveau_universitaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observation` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss` float DEFAULT NULL,
  `salaire` float DEFAULT NULL,
  `situation_familiale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sup` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexe` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_naissance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fonctionnaire`
--

INSERT INTO `fonctionnaire` (`id`, `compte`, `mot_de_passe`, `adresse`, `nom`, `specialite`, `telephone`, `role_id`, `affectation`, `chr`, `classification`, `conges`, `date_entree`, `horaire`, `i_acte`, `irg`, `mode_paiement`, `mode_recrutement`, `n_compte`, `n_ss`, `niveau_universitaire`, `observation`, `photo`, `prenom`, `rss`, `salaire`, `situation_familiale`, `sup`, `titre`, `sexe`, `date_naissance`) VALUES
(1, 'user', 'b14361404c078ffd549c03db443c3fede2f3e534d73f78f77301ed97d4a436a9fd9db05ee8b325c0ad36438b43fec8510c204fc1c1edb21d0941c00e9e2c1ce2', NULL, 'DJEBBAR', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youcef', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'c', 'acc28db2beb7b42baa1cb0243d401ccb4e3fce44d7b02879a52799aadff541522d8822598b2fa664f9d5156c00c924805d75c3868bd56c2acb81d37e98e35adc', '', 'Réceptionniste', '', '', 2, '', NULL, '', NULL, NULL, '', NULL, NULL, '', '', '', '', '', '', '', 'c', NULL, NULL, '', NULL, '', '', NULL),
(11, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', NULL, 'admin', NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'suser', '9e516e36535df7d8bd52cfeb5754deffba68f0830a72c4b426024a7b184925521421e23b712a116aeecab6cba3807b8edf5e4bb1b1e3ce4e89989eff9735e63c', NULL, 'super', NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'administrateur', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'niboucha', 'b14361404c078ffd549c03db443c3fede2f3e534d73f78f77301ed97d4a436a9fd9db05ee8b325c0ad36438b43fec8510c204fc1c1edb21d0941c00e9e2c1ce2', NULL, 'NIBOUCHA', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mohammed', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `date_operation` datetime DEFAULT NULL,
  `observation` longtext,
  `paye` tinyint(4) DEFAULT NULL,
  `reduction` float DEFAULT NULL,
  `resume_clinique` varchar(255) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `fonctionnaire_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `consulation_id` int(11) DEFAULT NULL,
  `medecin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operation`
--

INSERT INTO `operation` (`id`, `date_operation`, `observation`, `paye`, `reduction`, `resume_clinique`, `total`, `fonctionnaire_id`, `patient_id`, `consulation_id`, `medecin_id`) VALUES
(1, '2019-02-28 00:00:00', '', NULL, NULL, '', NULL, 1, 1, 1, 1),
(2, '2019-02-28 00:00:00', '', NULL, NULL, '', NULL, 1, 1, 1, 1),
(3, '2019-02-28 00:00:00', '', NULL, NULL, '', NULL, 1, 1, 1, 1),
(4, '2019-02-28 00:00:00', '', NULL, NULL, '', NULL, 1, 1, 1, 13),
(5, '2019-02-28 00:00:00', '', NULL, NULL, '', NULL, 1, 1, 1, 13),
(6, '2019-04-22 10:10:45', '', NULL, 0, '', 550, 4, 7, 35, 1),
(7, '2019-04-22 10:13:20', '', NULL, 0, '', 150, 4, 8, NULL, 13),
(8, '2019-04-22 15:03:02', '', NULL, 0, '', 550, 4, 10, 44, 1),
(9, '2019-04-22 15:41:33', '', NULL, 0, '', 400, 4, 9, NULL, 1),
(10, '2019-06-24 06:04:47', '', NULL, 0, '', 400, 4, 11, 49, 1),
(11, '2019-06-24 06:15:14', '', NULL, 0, '', 550, 4, 11, 51, 1),
(12, '2019-06-30 11:21:46', '', NULL, 0, '', 400, 4, 1, NULL, 1),
(13, '2019-06-30 11:22:31', '', NULL, 0, '', 150, 4, 3, NULL, 1),
(14, '2019-06-30 11:39:15', '', NULL, 0, '', 150, 4, 11, NULL, 1),
(15, '2019-06-30 11:46:43', '', NULL, 0, '', 400, 4, 4, NULL, 1),
(16, '2019-07-07 06:21:57', '', NULL, 0, '', 400, 4, 1, 52, 1),
(17, '2019-07-07 07:07:35', '', NULL, 0, '', 400, 4, 12, NULL, 1),
(18, '2019-07-07 07:10:53', '', NULL, 0, '', 150, 4, 11, 53, 1),
(19, '2019-07-07 09:55:49', '', NULL, 0, '', 150, 4, 12, NULL, 1),
(20, '2019-07-07 10:08:00', '', NULL, 0, '', 400, 4, 1, NULL, 1),
(21, '2019-07-07 10:09:16', '', NULL, 0, '', 400, 4, 13, NULL, 1),
(22, '2019-07-09 12:38:17', '', NULL, 0, '', 400, 4, 1, NULL, 1),
(23, '2019-07-09 12:44:41', '', NULL, 0, '', 150, 4, 3, NULL, 1),
(24, '2019-07-09 12:44:48', '', NULL, 0, '', 150, 4, 1, NULL, 1),
(25, '2019-07-09 12:48:45', '', NULL, 0, '', 150, 4, 1, NULL, 1),
(26, '2019-07-09 12:49:02', '', NULL, 0, '', 400, 4, 3, NULL, 1),
(27, '2019-07-13 16:10:21', '', NULL, 0, '', 400, 4, 11, NULL, 1),
(28, '2019-07-23 11:00:55', '', NULL, 0, '', 400, 4, 1, NULL, 1),
(29, '2019-07-23 11:04:14', '', NULL, 0, '', 1200, 4, 1, 54, 1),
(30, '2019-07-23 11:06:06', '', NULL, 0, '', 150, 4, 12, 55, 1),
(31, '2019-07-23 11:21:53', '', NULL, 0, '', 400, 4, 14, NULL, 1),
(32, '2019-07-23 17:44:01', '', NULL, 0, '', 400, 4, 4, NULL, 1),
(33, '2019-07-23 17:45:43', '', NULL, 0, '', 400, 4, 7, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `adresse` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ayantDroit` tinyint(1) DEFAULT '0',
  `chronique` tinyint(1) DEFAULT '0',
  `cp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `groupage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lieu_naissance` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nom_arabe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nom_jeune_fille` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `numero_carte` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nss` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'numeroSecuriteSociale',
  `observation` text CHARACTER SET utf8,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `prenom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `prenom_arabe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `profession` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sexe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `situation_familliale` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `solde` double DEFAULT NULL,
  `taux_reduction` double DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `titre` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `traitement_en_cours` text CHARACTER SET utf8,
  `type_convention` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `antecedent_familliaux` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `antecedent_perso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reduction` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `adresse`, `ayantDroit`, `chronique`, `cp`, `date_creation`, `date_naissance`, `groupage`, `lieu_naissance`, `nom`, `nom_arabe`, `nom_jeune_fille`, `numero_carte`, `nss`, `observation`, `photo`, `prenom`, `prenom_arabe`, `profession`, `sexe`, `situation_familliale`, `solde`, `taux_reduction`, `telephone`, `titre`, `traitement_en_cours`, `type_convention`, `antecedent_familliaux`, `antecedent_perso`, `reduction`) VALUES
(1, '', NULL, NULL, '', '2019-01-30 16:50:47', '1997-01-30', '', '', 'SAAD', '', '', '', '', '', '', 'Abbes', '', '', 'g', '', NULL, NULL, '', '', '', '', '', '', NULL),
(2, '', NULL, 1, '', '2019-02-04 12:06:32', '2017-01-04', '', '', 'SAABAT', '', '', '', '', '', '', 'Wael', '', '', 'g', 'm', 0, NULL, '', '', '', '', '', '', 0),
(3, '', NULL, NULL, '', '2019-02-25 15:32:40', '1964-02-25', '', '', 'TAANI', '', '', '', '', '', '', 'Saadeddine', '', '', 'g', '', NULL, NULL, '', '', '', '', '', '', NULL),
(4, '', NULL, NULL, '', '2019-02-27 11:16:18', '1964-02-27', '', '', 'TEST', '', '', '', '', '', '', 'Test', '', '', '', '', NULL, NULL, '', '', '', '', '', '', NULL),
(5, '', NULL, NULL, '', '2019-04-22 10:01:45', '2007-04-22', '', '', 'TAAEN', '', '', '', '', '', '', 'Rabah akrem', '', '', 'g', '', NULL, NULL, '032162132', '', '', '', '', '', NULL),
(6, '', NULL, NULL, '', '2019-04-22 10:07:57', '2003-04-22', '', '', 'TAAEN', '', '', '', '', '', '', 'El bachir', '', '', 'g', '', NULL, NULL, '03362123', '', '', '', '', '', NULL),
(7, '', NULL, NULL, '', '2019-04-22 10:10:31', '1983-04-22', '', '', 'YAAKOUBI', '', '', '', '', '', '', 'Ebdelhadi', '', '', 'g', '', NULL, NULL, '033621245', '', '', '', '', '', NULL),
(8, '', NULL, NULL, '', '2019-04-22 10:12:58', '1989-04-22', '', '', 'FAFA', '', '', '', '', '', '', 'Chabane ben abdelhamid', '', '', 'g', '', NULL, NULL, '033631231', '', '', '', '', '', NULL),
(9, '', NULL, NULL, '', '2019-04-22 13:43:44', '1994-04-22', '', '', 'CEHRITIA', '', '', '', '', '', '', 'El amine', '', '', 'g', '', NULL, NULL, '', '', '', '', '', '', NULL),
(10, '', NULL, NULL, '', '2019-04-22 15:02:32', '1994-04-22', '', '', 'EDDAIKRA', '', '', '', '', '', '', 'Yaakoub', '', '', 'g', '', NULL, NULL, '033629612', '', '', '', '', '', NULL),
(11, '', NULL, NULL, '', '2019-06-24 06:04:13', '1987-06-24', '', '', 'DAAOUH', '', '', '', '', '', '', 'Zaidi', '', '', 'g', '', NULL, NULL, '033821290', '', '', '', '', '', NULL),
(12, '', NULL, NULL, '', '2019-07-07 07:07:23', '1987-07-07', '', '', 'AAMER', '', '', '', '', '', '', 'Dahman yacine', '', '', 'g', '', NULL, NULL, '06321321', '', '', '', '', '', NULL),
(13, '', NULL, NULL, '', '2019-07-07 10:09:04', '1998-07-07', '', '', 'SAABAT', '', '', '', '', '', '', 'Maamar', '', '', 'g', '', NULL, NULL, '03663212', '', '', '', '', '', NULL),
(14, '', NULL, NULL, '', '2019-07-23 11:21:06', '1989-07-23', '', '', 'PIZZI', '', '', '', '', '', '', 'Nacef', '', '', 'g', '', NULL, NULL, '033821260', '', '', '', '', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acte`
--
ALTER TABLE `acte`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_consultation_patient` (`patient`);

--
-- Indexes for table `detail_operation`
--
ALTER TABLE `detail_operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operation_id` (`operation_id`),
  ADD KEY `acte_id` (`acte_id`),
  ADD KEY `medecin_id` (`medecin_id`);

--
-- Indexes for table `fonctionnaire`
--
ALTER TABLE `fonctionnaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_fonctionnaire_Role1_idx` (`role_id`);

--
-- Indexes for table `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fonctionnaire_id` (`fonctionnaire_id`),
  ADD KEY `patient_id` (`patient_id`),
  ADD KEY `consulation_id` (`consulation_id`),
  ADD KEY `medecin_id` (`medecin_id`),
  ADD KEY `medecin_id_2` (`medecin_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acte`
--
ALTER TABLE `acte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `detail_operation`
--
ALTER TABLE `detail_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `fonctionnaire`
--
ALTER TABLE `fonctionnaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acte`
--


--
-- Constraints for table `consultation`
--
ALTER TABLE `consultation`
  ADD CONSTRAINT `FK_consultation_patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`);

--
-- Constraints for table `detail_operation`
--
ALTER TABLE `detail_operation`
  ADD CONSTRAINT `FK9jqydxq0n4f9mlg2u3tlr3h60` FOREIGN KEY (`acte_id`) REFERENCES `acte` (`id`),
  ADD CONSTRAINT `FKchrewjs5s5iiqrt4fny9byohw` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`),
  ADD CONSTRAINT `detail_operation_ibfk_1` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`),
  ADD CONSTRAINT `detail_operation_ibfk_2` FOREIGN KEY (`acte_id`) REFERENCES `acte` (`id`),
  ADD CONSTRAINT `fk_medecin` FOREIGN KEY (`medecin_id`) REFERENCES `fonctionnaire` (`id`);

--
-- Constraints for table `fonctionnaire`
--

--
-- Constraints for table `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `FK7rd0mfae3yh72ouihsuahbc8k` FOREIGN KEY (`consulation_id`) REFERENCES `consultation` (`id`),
  ADD CONSTRAINT `FKbnx8mrrnjvq9v0urw935v1400` FOREIGN KEY (`fonctionnaire_id`) REFERENCES `fonctionnaire` (`id`),
  ADD CONSTRAINT `FKjl6iwi875dn3ytmmc7k06fxh5` FOREIGN KEY (`medecin_id`) REFERENCES `fonctionnaire` (`id`),
  ADD CONSTRAINT `FKrnhrme2dycdtl3tp2ibiyjyh` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  ADD CONSTRAINT `operation_ibfk_1` FOREIGN KEY (`fonctionnaire_id`) REFERENCES `fonctionnaire` (`id`),
  ADD CONSTRAINT `operation_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  ADD CONSTRAINT `operation_ibfk_3` FOREIGN KEY (`consulation_id`) REFERENCES `consultation` (`id`),
  ADD CONSTRAINT `operation_ibfk_4` FOREIGN KEY (`medecin_id`) REFERENCES `fonctionnaire` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
