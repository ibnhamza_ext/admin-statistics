package com.ibnhamza.ihsaai.controller;

import Hibernate.HibernateUtil;
import Hibernate.OperationStat;
import Hibernate.PatientStat;
import JsonObjects.OperationByActe;
import JsonObjects.OperationByAnnée;
import JsonObjects.OperationByMedecin;
import JsonObjects.OperationByMonth;
import JsonObjects.PatientByAgeRange;
import JsonObjects.PatientBySexe;
import com.google.gson.Gson;
import com.ibnhamza.ihsaai.entity.Acte;
import com.ibnhamza.ihsaai.entity.Fonctionnaire;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.sun.glass.events.MouseEvent;
import ibnhamza.rest.api.HttpClient;
import ibnhamza.rest.api.HttpResponse;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterAttributes;
import javafx.print.PrinterJob;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import static javafx.scene.input.KeyCode.S;
import static javafx.scene.input.KeyCode.T;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.sound.midi.SysexMessage;
import javax.swing.JButton;
import javax.swing.JTable;

public class MainController implements Initializable {

    @FXML
    private JFXComboBox cbTable;
    @FXML
    private CategoryAxis categoryaxis;
    @FXML
    private CategoryAxis categoryAxisLineChart;
    @FXML
    private JFXComboBox cbParameter;

    @FXML
    private PieChart piechart;

    @FXML
    private BarChart barchart;

    @FXML
    private LineChart linechart;

    @FXML
    private JFXDatePicker start;

    @FXML
    private JFXDatePicker end;

    @FXML
    private JFXButton print;

    @FXML
    private HBox graph_layout;

    private String selectedTable;

    public PatientStat patientstat;
    public OperationStat operationStat;

    public Label startPeriod;
    public Label endPeriod;

    private HttpClient http;

    private List<String> trancheAge;

    public int piechart_status;
    public int barchart_status;
    public int linechart_status;
    public String param_pie_or_bar = "";
    public String param_line = "";
    public TableView tableView1;
    public TableView tableView2;
    public TableView tableView3;
    public TableView tableView41;
    public TableView tableView42;
    public TableView tableView5;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        List<String> trancheAge = new ArrayList();
        trancheAge.add("0-18");
        trancheAge.add(">18");

        patientstat = new PatientStat();
        operationStat = new OperationStat();
        initComboBox();

        start.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                /* DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate d = LocalDate.parse(newValue.toString(), formatter);*/
                if (cbTable.getSelectionModel().getSelectedItem() != null && cbParameter.getSelectionModel().getSelectedItem() != null) {
                    if (end.valueProperty().get() != null) {
                        try {
                            operationByPeriod(cbParameter.valueProperty().get(), newValue, end.valueProperty().get());
                            operationByMedecin(cbParameter.valueProperty().get(), newValue, end.valueProperty().get());
                            try {
                                operationByConsultation(cbParameter.valueProperty().get(), newValue, end.valueProperty().get());
                            } catch (IOException ex) {
                                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            operationByPeriod(cbParameter.valueProperty().get(), newValue, LocalDate.now());
                            operationByMedecin(cbParameter.valueProperty().get(), newValue, LocalDate.now());
                            try {
                                operationByConsultation(cbParameter.valueProperty().get(), newValue, LocalDate.now());
                            } catch (IOException ex) {
                                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                //  System.out.print(end.valueProperty().get().getDayOfYear());

            }
        });

        end.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                if (cbTable.getSelectionModel().getSelectedItem() != null && cbParameter.getSelectionModel().getSelectedItem() != null) {
                    if (start.valueProperty().get() != null) {
                        try {
                            operationByPeriod(cbParameter.valueProperty().get(), start.valueProperty().get(), newValue);
                            operationByMedecin(cbParameter.valueProperty().get(), start.valueProperty().get(), newValue);
                            try {
                                operationByConsultation(cbParameter.valueProperty().get(), start.valueProperty().get(), newValue);
                            } catch (IOException ex) {
                                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.print("waiting to enter the start date");
                    }
                }
            }
        });

        cbParameter.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                try {
                    patientSexeHandler(newValue);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    patientAgeRangeHandler(newValue);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (start.valueProperty().get() != null && end.valueProperty().get() == null) {
                    try {
                        operationByPeriod(newValue, start.valueProperty().get(), LocalDate.now());
                        operationByMedecin(newValue, start.valueProperty().get(), LocalDate.now());
                        try {
                            operationByConsultation(newValue, start.valueProperty().get(), LocalDate.now());
                        } catch (IOException ex) {
                            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (start.valueProperty().get() != null && end.valueProperty().get() != null) {
                    try {
                        operationByPeriod(newValue, start.valueProperty().get(), end.valueProperty().get());
                        operationByMedecin(newValue, start.valueProperty().get(), end.valueProperty().get());
                        try {
                            operationByConsultation(newValue, start.valueProperty().get(), end.valueProperty().get());
                        } catch (IOException ex) {
                            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        operationByPeriod(newValue);
                        operationByMedecin(newValue);
                        operationByConsultation(newValue);
                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

    }

    public void initTable() {

        tableView1 = new TableView();

        TableColumn<String, PatientBySexe> column11 = new TableColumn<>("Sexe");
        column11.setCellValueFactory(new PropertyValueFactory<>("Sexe"));

        TableColumn<String, PatientBySexe> column12 = new TableColumn<>("Number des Patients");
        column12.setCellValueFactory(new PropertyValueFactory<>("number_of_operation"));

        tableView1.getColumns().add(column11);
        tableView1.getColumns().add(column12);
        /*  ObservableList<PatientBySexe> patient_by_sexe = FXCollections.observableArrayList();
        patient_by_sexe.add(new PatientBySexe("Homme", 10));
        patient_by_sexe.add(new PatientBySexe("Homme", 10));
        patient_by_sexe.add(new PatientBySexe("Homme", 10));

        tableView1.setItems(patient_by_sexe);*/

        tableView2 = new TableView();

        TableColumn<String, PatientBySexe> column21 = new TableColumn<>("Age Range");
        column21.setCellValueFactory(new PropertyValueFactory<>("ageRange"));

        TableColumn<String, PatientBySexe> column22 = new TableColumn<>("Number des Patients");
        column22.setCellValueFactory(new PropertyValueFactory<>("number_of_patient"));

        tableView2.getColumns().add(column21);
        tableView2.getColumns().add(column22);

        tableView3 = new TableView();

        TableColumn<String, PatientBySexe> column31 = new TableColumn<>("Nom");
        column31.setCellValueFactory(new PropertyValueFactory<>("nom"));

        TableColumn<String, PatientBySexe> column32 = new TableColumn<>("prénom");
        column32.setCellValueFactory(new PropertyValueFactory<>("prénom"));

        TableColumn<String, PatientBySexe> column33 = new TableColumn<>("Number des Operation");
        column33.setCellValueFactory(new PropertyValueFactory<>("number_of_operation"));

        tableView3.getColumns().add(column31);
        tableView3.getColumns().add(column32);
        tableView3.getColumns().add(column33);

        tableView41 = new TableView();

        TableColumn<String, PatientBySexe> column411 = new TableColumn<>("Année");
        column411.setCellValueFactory(new PropertyValueFactory<>("Année"));

        TableColumn<String, PatientBySexe> column412 = new TableColumn<>("Number des Operation");
        column412.setCellValueFactory(new PropertyValueFactory<>("nombre_operation"));

        tableView41.getColumns().add(column411);
        tableView41.getColumns().add(column412);

        tableView42 = new TableView();

        TableColumn<String, PatientBySexe> column421 = new TableColumn<>("Année");
        column421.setCellValueFactory(new PropertyValueFactory<>("Année"));

        TableColumn<String, PatientBySexe> column422 = new TableColumn<>("Mois");
        column422.setCellValueFactory(new PropertyValueFactory<>("Mois"));

        TableColumn<String, PatientBySexe> column423 = new TableColumn<>("Number des Operation");
        column423.setCellValueFactory(new PropertyValueFactory<>("nombre_operation"));

        tableView42.getColumns().add(column421);
        tableView42.getColumns().add(column422);
        tableView42.getColumns().add(column423);

        tableView5 = new TableView();

        TableColumn<String, PatientBySexe> column51 = new TableColumn<>("description");
        column51.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<String, PatientBySexe> column52 = new TableColumn<>("Number des Operation");
        column52.setCellValueFactory(new PropertyValueFactory<>("nombre_operation"));

        tableView5.getColumns().add(column51);
        tableView5.getColumns().add(column52);

        tableView1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView2.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView3.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView41.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView42.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView5.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    public void print_table_view(TableView table_view, PrinterJob job, javafx.print.PageLayout pageLayout) {

        double pagePrintableWidth = pageLayout.getPrintableWidth(); //this should be 8.5 inches for this page layout.
        double pagePrintableHeight = pageLayout.getPrintableHeight();// this should be 11 inches for this page layout.
        // MainController.this.tableView1.setFixedCellSize(50);
        // MainController.this.tableView1.setFixedCellSize(MainController.this.tableView1.getItems().size());
        table_view.prefHeightProperty().bind(
                Bindings.size(table_view.getItems())
                .multiply(50)
        );// If your cells' rows are variable size you add the .multiply and play with the input value until your output is close to what you want. If your cells' rows are the same height, I think you can use .multiply(1). This changes the height of your tableView to show all rows in the table.
        table_view.minHeightProperty().bind(table_view.prefHeightProperty());//You can probably play with this to see if it's really needed.  Comment it out to find out.
        table_view.maxHeightProperty().bind(table_view.prefHeightProperty());//You can probably play with this to see if it' really needed.  Comment it out to find out.

        double scaleX = pagePrintableWidth / table_view.getBoundsInParent().getWidth();//scaling down so that the printing width fits within the paper's width bound.
        double scaleY = scaleX; //scaling the height using the same scale as the width. This allows the writing and the images to maintain their scale, or not look skewed.
        double localScale = scaleX; //not really needed since everything is scaled down at the same ratio. scaleX is used thoughout the program to scale the print out.

        double numberOfPages = Math.ceil((table_view.getPrefHeight() * localScale) / pagePrintableHeight);//used to figure out the number of pages that will be printed.

        table_view.getTransforms().add(new Scale(scaleX, (scaleY)));//scales the printing. Allowing the width to say within the papers width, and scales the height to do away with skewed letters and images.
        table_view.getTransforms().add(new Translate(0, 0));// starts the first print at the top left corner of the image that needs to be printed

        //Since the height of what needs to be printed is longer than the paper's heights we use gridTransfrom to only select the part to be printed for a given page.
        Translate gridTransform = new Translate();
        table_view.getTransforms().add(gridTransform);
        for (int i = 0; i < numberOfPages; i++) {
            gridTransform.setY(-i * (pagePrintableHeight / localScale));
            job.printPage(pageLayout, table_view);
        }

    }

    @FXML
    private void printActionListener(ActionEvent e) throws IOException {

        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(graph_layout.getScene().getWindow());

        VBox dialogVbox = new VBox(20);
        JFXDatePicker start_picker = new JFXDatePicker();
        JFXDatePicker end_picker = new JFXDatePicker();
        JFXButton btn = new JFXButton("Submit");
        dialogVbox.getChildren().add(start_picker);
        dialogVbox.getChildren().add(end_picker);
        dialogVbox.getChildren().add(btn);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.setPadding(new Insets(30, 30, 30, 30));
        Scene dialogScene = new Scene(dialogVbox);
        dialog.setScene(dialogScene);
        dialog.show();

        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (end_picker.valueProperty().get() == null && start_picker.valueProperty().get() != null) {

                    dialog.hide();

                    Printer printer = Printer.getDefaultPrinter();
                    PageLayout pageLayout
                            = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);

                    VBox v = new VBox();

                    VBox v1 = new VBox();
                    CategoryAxis xAxis = new CategoryAxis();
                    NumberAxis yAxis = new NumberAxis();

                    BarChart b = new BarChart(xAxis, yAxis);
                    PieChart p = new PieChart();

                    CategoryAxis xAxis4 = new CategoryAxis();
                    NumberAxis yAxis4 = new NumberAxis();
                    LineChart l = new LineChart(xAxis4, yAxis4);

                    JFXButton print_btn = new JFXButton("Print");
                    HBox h = new HBox();
                    h.getChildren().add(print_btn);
                    h.setAlignment(Pos.CENTER);
                    HBox.setMargin(print_btn, new Insets(10, 1, 1, 1));
                    v.getChildren().add(h);

                    v.getChildren().add(v1);

                    ScrollPane scrollPane = new ScrollPane(v);
                    scrollPane.setFitToWidth(true);
                    Scene s = new Scene(scrollPane, pageLayout.getPrintableWidth(), pageLayout.getPrintableHeight());

                    Stage newWindow = new Stage();
                    newWindow.setScene(s);

                    MainController.this.initTable();

                    print_btn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            // MainController.this.traverse(MainController.this.tableView1, piechart_status, piechart_status);

                            newWindow.hide();

                            Printer printer = Printer.getDefaultPrinter(); //get the default printer
                            javafx.print.PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT); //create a pagelayout.  I used Paper.NA_LETTER for a standard 8.5 x 11 in page.
                            PrinterJob job = PrinterJob.createPrinterJob();//create a printer job

                            if (job.showPrintDialog(graph_layout.getScene().getWindow())) {

                                VBox vb = new VBox();
                                VBox vp = new VBox();
                                VBox vl = new VBox();
                                if (b.getData().size() != 0) {
                                    vb.getChildren().add(b);
                                    job.printPage(pageLayout, vb);
                                }

                                if (p.getData().size() != 0) {
                                    vp.getChildren().add(p);
                                    job.printPage(pageLayout, vp);
                                }

                                if (MainController.this.tableView1.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView1, job, pageLayout);
                                }

                                if (MainController.this.tableView2.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView2, job, pageLayout);
                                }

                                if (MainController.this.tableView3.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView3, job, pageLayout);
                                }

                                if (MainController.this.tableView5.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView5, job, pageLayout);
                                }

                                if (l.getData().size() != 0) {
                                    vl.getChildren().add(l);
                                    job.printPage(pageLayout, vl);
                                }

                                if (MainController.this.tableView41.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView41, job, pageLayout);
                                }

                                if (MainController.this.tableView42.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView42, job, pageLayout);
                                }

                                job.endJob();//finally end the printing job.
                            }

                        }
                    });

                    try {
                        if (MainController.this.barchart_status == 1 && MainController.this.piechart_status == 1) {
                            MainController.this.patientSexeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 2 && MainController.this.piechart_status == 2) {
                            MainController.this.patientAgeRangeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 3 && MainController.this.piechart_status == 3) {
                            MainController.this.operationByMedecin(MainController.this.param_pie_or_bar,
                                    start_picker.valueProperty().get(),
                                    LocalDate.now(),
                                    b,
                                    p);
                        }
                        if (MainController.this.linechart_status == 1) {
                            MainController.this.operationByPeriod(MainController.this.param_line,
                                    start_picker.valueProperty().get(),
                                    LocalDate.now(),
                                    l);
                        }
                        if (MainController.this.barchart_status == 4 && MainController.this.piechart_status == 4) {
                            MainController.this.operationByConsultation(MainController.this.param_pie_or_bar,
                                    start_picker.valueProperty().get(),
                                    LocalDate.now(),
                                    b,
                                    p);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (MainController.this.tableView1.getItems().size() != 0) {

                        VBox htableView1 = new VBox();
                        VBox.setMargin(htableView1, new Insets(10, 10, 10, 10));
                        htableView1.getChildren().add(tableView1);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView1);

                    }
                    if (MainController.this.tableView2.getItems().size() != 0) {
                        VBox htableView2 = new VBox();
                        VBox.setMargin(htableView2, new Insets(10, 10, 10, 10));
                        htableView2.getChildren().add(tableView2);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView2);

                    }

                    if (MainController.this.tableView3.getItems().size() != 0) {
                        VBox htableView3 = new VBox();
                        VBox.setMargin(htableView3, new Insets(10, 10, 10, 10));
                        htableView3.getChildren().add(tableView3);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView3);

                    }

                    if (MainController.this.tableView41.getItems().size() != 0) {
                        VBox htableView41 = new VBox();
                        VBox.setMargin(htableView41, new Insets(10, 10, 10, 10));
                        htableView41.getChildren().add(tableView41);
                        v1.getChildren().add(l);
                        v1.getChildren().add(htableView41);

                    }

                    if (MainController.this.tableView42.getItems().size() != 0) {
                        VBox htableView42 = new VBox();
                        VBox.setMargin(htableView42, new Insets(10, 10, 10, 10));
                        htableView42.getChildren().add(tableView42);
                        v1.getChildren().add(l);
                        v1.getChildren().add(htableView42);

                    }

                    if (MainController.this.tableView5.getItems().size() != 0) {
                        VBox htableView5 = new VBox();
                        VBox.setMargin(htableView5, new Insets(10, 10, 10, 10));
                        htableView5.getChildren().add(tableView5);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView5);

                    }
                    newWindow.show();
                } else if (end_picker.valueProperty()
                        .get() != null && start_picker.valueProperty().get() != null) {

                    dialog.hide();

                    Printer printer = Printer.getDefaultPrinter();
                    PageLayout pageLayout
                            = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);

                    VBox v = new VBox();

                    VBox v1 = new VBox();
                    CategoryAxis xAxis = new CategoryAxis();
                    NumberAxis yAxis = new NumberAxis();

                    BarChart b = new BarChart(xAxis, yAxis);
                    PieChart p = new PieChart();

                    CategoryAxis xAxis4 = new CategoryAxis();
                    NumberAxis yAxis4 = new NumberAxis();
                    LineChart l = new LineChart(xAxis4, yAxis4);

                    JFXButton print_btn = new JFXButton("Print");
                    HBox h = new HBox();
                    h.getChildren().add(print_btn);
                    h.setAlignment(Pos.CENTER);
                    HBox.setMargin(print_btn, new Insets(10, 1, 1, 1));
                    v.getChildren().add(h);

                    v.getChildren().add(v1);

                    ScrollPane scrollPane = new ScrollPane(v);
                    scrollPane.setFitToWidth(true);
                    Scene s = new Scene(scrollPane, pageLayout.getPrintableWidth(), pageLayout.getPrintableHeight());

                    Stage newWindow = new Stage();
                    newWindow.setScene(s);
                    MainController.this.initTable();

                    print_btn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {

                            newWindow.hide();

                            Printer printer = Printer.getDefaultPrinter(); //get the default printer
                            javafx.print.PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT); //create a pagelayout.  I used Paper.NA_LETTER for a standard 8.5 x 11 in page.
                            PrinterJob job = PrinterJob.createPrinterJob();//create a printer job

                            if (job.showPrintDialog(graph_layout.getScene().getWindow())) {

                                VBox vb = new VBox();
                                VBox vp = new VBox();
                                VBox vl = new VBox();
                                if (b.getData().size() != 0) {
                                    vb.getChildren().add(b);
                                    job.printPage(pageLayout, vb);
                                }

                                if (p.getData().size() != 0) {
                                    vp.getChildren().add(p);
                                    job.printPage(pageLayout, vp);
                                }

                                if (MainController.this.tableView1.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView1, job, pageLayout);
                                }

                                if (MainController.this.tableView2.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView2, job, pageLayout);
                                }

                                if (MainController.this.tableView3.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView3, job, pageLayout);
                                }

                                if (MainController.this.tableView5.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView5, job, pageLayout);
                                }

                                if (l.getData().size() != 0) {
                                    vl.getChildren().add(l);
                                    job.printPage(pageLayout, vl);
                                }

                                if (MainController.this.tableView41.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView41, job, pageLayout);
                                }

                                if (MainController.this.tableView42.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView42, job, pageLayout);
                                }

                                job.endJob();//finally end the printing job.
                            }

                        }
                    });

                    try {
                        if (MainController.this.barchart_status == 1 && MainController.this.piechart_status == 1) {
                            MainController.this.patientSexeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 2 && MainController.this.piechart_status == 2) {
                            MainController.this.patientAgeRangeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 3 && MainController.this.piechart_status == 3) {
                            MainController.this.operationByMedecin(MainController.this.param_pie_or_bar,
                                    start_picker.valueProperty().get(),
                                    end_picker.valueProperty().get(),
                                    b,
                                    p);
                        }
                        if (MainController.this.linechart_status == 1) {
                            MainController.this.operationByPeriod(MainController.this.param_line,
                                    start_picker.valueProperty().get(),
                                    end_picker.valueProperty().get(),
                                    l);
                        }
                        if (MainController.this.barchart_status == 4 && MainController.this.piechart_status == 4) {
                            MainController.this.operationByConsultation(MainController.this.param_pie_or_bar,
                                    start_picker.valueProperty().get(),
                                    end_picker.valueProperty().get(),
                                    b,
                                    p);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (MainController.this.tableView1.getItems().size() != 0) {

                        VBox htableView1 = new VBox();
                        VBox.setMargin(htableView1, new Insets(10, 10, 10, 10));
                        htableView1.getChildren().add(tableView1);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView1);

                    }
                    if (MainController.this.tableView2.getItems().size() != 0) {
                        VBox htableView2 = new VBox();
                        VBox.setMargin(htableView2, new Insets(10, 10, 10, 10));
                        htableView2.getChildren().add(tableView2);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView2);

                    }

                    if (MainController.this.tableView3.getItems().size() != 0) {
                        VBox htableView3 = new VBox();
                        VBox.setMargin(htableView3, new Insets(10, 10, 10, 10));
                        htableView3.getChildren().add(tableView3);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);

                        v1.getChildren().add(htableView3);

                    }

                    if (MainController.this.tableView41.getItems().size() != 0) {
                        VBox htableView41 = new VBox();
                        VBox.setMargin(htableView41, new Insets(10, 10, 10, 10));
                        v1.getChildren().add(htableView41);
                        v1.getChildren().add(l);
                        htableView41.getChildren().add(tableView41);

                    }

                    if (MainController.this.tableView42.getItems().size() != 0) {
                        VBox htableView42 = new VBox();
                        VBox.setMargin(htableView42, new Insets(10, 10, 10, 10));
                        htableView42.getChildren().add(tableView42);
                        v1.getChildren().add(l);
                        v1.getChildren().add(htableView42);

                    }

                    if (MainController.this.tableView5.getItems().size() != 0) {
                        VBox htableView5 = new VBox();
                        VBox.setMargin(htableView5, new Insets(10, 10, 10, 10));
                        htableView5.getChildren().add(tableView5);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView5);

                    }
                    newWindow.show();

                } else if (end_picker.valueProperty()
                        .get() == null && start_picker.valueProperty().get() == null) {
                    LocalDate st, ed;
                    if (start.valueProperty().get() != null) {
                        st = start.valueProperty().get();
                    } else {
                        st = null;

                    }

                    if (end.valueProperty().get() != null) {
                        ed = end.valueProperty().get();
                    } else {

                        ed = LocalDate.now();

                    }

                    dialog.hide();

                    Printer printer = Printer.getDefaultPrinter();
                    PageLayout pageLayout
                            = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);

                    VBox v = new VBox();

                    VBox v1 = new VBox();
                    CategoryAxis xAxis = new CategoryAxis();
                    NumberAxis yAxis = new NumberAxis();

                    BarChart b = new BarChart(xAxis, yAxis);
                    PieChart p = new PieChart();

                    CategoryAxis xAxis4 = new CategoryAxis();
                    NumberAxis yAxis4 = new NumberAxis();
                    LineChart l = new LineChart(xAxis4, yAxis4);

                    JFXButton print_btn = new JFXButton("Print");
                    HBox h = new HBox();
                    h.getChildren().add(print_btn);
                    h.setAlignment(Pos.CENTER);
                    HBox.setMargin(print_btn, new Insets(10, 1, 1, 1));
                    v.getChildren().add(h);

                    v.getChildren().add(v1);

                    ScrollPane scrollPane = new ScrollPane(v);
                    scrollPane.setFitToWidth(true);
                    Scene s = new Scene(scrollPane, pageLayout.getPrintableWidth(), pageLayout.getPrintableHeight());

                    Stage newWindow = new Stage();
                    newWindow.setScene(s);
                    MainController.this.initTable();

                    print_btn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {

                            newWindow.hide();

                            Printer printer = Printer.getDefaultPrinter(); //get the default printer
                            javafx.print.PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT); //create a pagelayout.  I used Paper.NA_LETTER for a standard 8.5 x 11 in page.
                            PrinterJob job = PrinterJob.createPrinterJob();//create a printer job

                            if (job.showPrintDialog(graph_layout.getScene().getWindow())) {

                                VBox vb = new VBox();
                                VBox vp = new VBox();
                                VBox vl = new VBox();
                                if (b.getData().size() != 0) {
                                    vb.getChildren().add(b);
                                    job.printPage(pageLayout, vb);
                                }

                                if (p.getData().size() != 0) {
                                    vp.getChildren().add(p);
                                    job.printPage(pageLayout, vp);
                                }

                                if (MainController.this.tableView1.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView1, job, pageLayout);
                                }

                                if (MainController.this.tableView2.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView2, job, pageLayout);
                                }

                                if (MainController.this.tableView3.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView3, job, pageLayout);
                                }

                                if (MainController.this.tableView5.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView5, job, pageLayout);
                                }

                                if (l.getData().size() != 0) {
                                    vl.getChildren().add(l);
                                    job.printPage(pageLayout, vl);
                                }

                                if (MainController.this.tableView41.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView41, job, pageLayout);
                                }

                                if (MainController.this.tableView42.getItems().size() != 0) {
                                    MainController.this.print_table_view(MainController.this.tableView42, job, pageLayout);
                                }

                                job.endJob();//finally end the printing job.
                            }

                        }
                    });

                    try {
                        if (MainController.this.barchart_status == 1 && MainController.this.piechart_status == 1) {
                            MainController.this.patientSexeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 2 && MainController.this.piechart_status == 2) {
                            MainController.this.patientAgeRangeHandler(MainController.this.param_pie_or_bar, b, p);
                        }

                        if (MainController.this.barchart_status == 3 && MainController.this.piechart_status == 3) {
                            MainController.this.operationByMedecin(MainController.this.param_pie_or_bar,
                                    st,
                                    ed,
                                    b,
                                    p);
                        }
                        if (MainController.this.linechart_status == 1) {
                            MainController.this.operationByPeriod(MainController.this.param_line,
                                    st,
                                    ed,
                                    l);
                        }
                        if (MainController.this.barchart_status == 4 && MainController.this.piechart_status == 4) {
                            MainController.this.operationByConsultation(MainController.this.param_pie_or_bar,
                                    st,
                                    ed,
                                    b,
                                    p);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (MainController.this.tableView1.getItems().size() != 0) {

                        VBox htableView1 = new VBox();
                        VBox.setMargin(htableView1, new Insets(10, 10, 10, 10));
                        htableView1.getChildren().add(tableView1);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView1);

                    }
                    if (MainController.this.tableView2.getItems().size() != 0) {
                        VBox htableView2 = new VBox();
                        VBox.setMargin(htableView2, new Insets(10, 10, 10, 10));
                        htableView2.getChildren().add(tableView2);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView2);

                    }

                    if (MainController.this.tableView3.getItems().size() != 0) {
                        VBox htableView3 = new VBox();
                        VBox.setMargin(htableView3, new Insets(10, 10, 10, 10));
                        htableView3.getChildren().add(tableView3);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView3);

                    }

                    if (MainController.this.tableView41.getItems().size() != 0) {
                        VBox htableView41 = new VBox();
                        VBox.setMargin(htableView41, new Insets(10, 10, 10, 10));
                        htableView41.getChildren().add(tableView41);
                        v1.getChildren().add(l);
                        v1.getChildren().add(htableView41);

                    }

                    if (MainController.this.tableView42.getItems().size() != 0) {
                        VBox htableView42 = new VBox();
                        VBox.setMargin(htableView42, new Insets(10, 10, 10, 10));
                        htableView42.getChildren().add(tableView42);
                        v1.getChildren().add(l);

                        v1.getChildren().add(htableView42);

                    }

                    if (MainController.this.tableView5.getItems().size() != 0) {
                        VBox htableView5 = new VBox();
                        VBox.setMargin(htableView5, new Insets(10, 10, 10, 10));
                        htableView5.getChildren().add(tableView5);
                        v1.getChildren().add(b);
                        v1.getChildren().add(p);
                        v1.getChildren().add(htableView5);

                    }
                    newWindow.show();

                }

            }
        });

    }

    public void preview(LocalDate start, LocalDate end) throws IOException {

        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout
                = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.HARDWARE_MINIMUM);

        VBox v = new VBox();
        VBox v1 = new VBox();
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        /* xAxis.setLabel("Devices");*/
        BarChart b = new BarChart(xAxis, yAxis);
        PieChart p = new PieChart();
        BarChart b1 = new BarChart(xAxis, yAxis);
        PieChart p1 = new PieChart();

        v1.getChildren().add(b);
        v1.getChildren().add(p);
        v1.getChildren().add(b1);
        v1.getChildren().add(p1);

        v.getChildren().add(v1);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(v);
        Scene s = new Scene(scrollPane, pageLayout.getPrintableWidth(), pageLayout.getPrintableHeight());
        Stage newWindow = new Stage();
        newWindow.setScene(s);
        this.patientSexeHandler("tout", b, p);

        this.patientAgeRangeHandler("tout", b1, p1);

        newWindow.show();

    }

    private void initComboBox() {
        //init http client 

        cbTable.getItems().addAll("Patient-Sexe",
                "Patient-DateDeNaissance",
                "Operation-Medecin",
                "Operation-Date operation",
                "Operation-Acte");

        cbTable.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                System.out.print(newValue);

                /*  start.getEditor().clear();
                end.getEditor().clear();*/
                if (newValue.toString().equals("Patient-Sexe")) {
                    //   piechart.getData().clear();
                    //  barchart.getData().clear();

                    //  categoryaxis.getCategories().clear();
                    // piechart.setTitle("REPRESENTATION");
                    //     barchart.setTitle("CATEGORIES");
                    cbParameter.getItems().clear();
                    cbParameter.getItems().addAll("Tout", "Homme", "Femme");
                } else if (newValue.toString().equals("Patient-DateDeNaissance")) {
                    // piechart.getData().clear();
                    // barchart.getData().clear();
                    // linechart.getData().clear();
                    //  categoryAxisLineChart.getCategories().clear();
                    //  linechart.setTitle("EVOLUTION");
                    // categoryaxis.getCategories().clear();
                    //  piechart.setTitle("REPRESENTATION");
                    //  barchart.setTitle("CATEGORIES");

                    trancheAge = new ArrayList<String>();
                    trancheAge.add("0-18");
                    trancheAge.add(">18");
                    cbParameter.getItems().clear();
                    cbParameter.getItems().add("Tout");

                    // for(int i=0;i<MainController.this.trancheAge.size();i++)  cbParameter.getItems().add(MainController.this.trancheAge.get(i));
                    for (String patientRange : trancheAge) {
                        //  System.out.println(".changed()"+patientRange);
                        cbParameter.getItems().add(patientRange.toString());
                    }
                } else if (newValue.toString().equals("Operation-Medecin")) {

                    //  piechart.getData().clear();
                    //    barchart.getData().clear();
                    //   linechart.getData().clear();
                    //   categoryAxisLineChart.getCategories().clear();
                    //   linechart.setTitle("EVOLUTION");
                    //   categoryaxis.getCategories().clear();
                    // piechart.setTitle("REPRESENTATION");
                    //   barchart.setTitle("CATEGORIES");
                    List<Fonctionnaire> ListMedecins = null;
                    ListMedecins = operationStat.getMedcins();
                    cbParameter.getItems().clear();
                    cbParameter.getItems().add("Tout");

                    for (Fonctionnaire Medecin : ListMedecins) {

                        cbParameter.getItems().add(Medecin.getNom() + " " + Medecin.getPrenom());

                    }

                } else if (newValue.toString().equals("Operation-Date operation")) {
                    //  piechart.getData().clear();
                    //  barchart.getData().clear();
                    //   linechart.getData().clear();
                    //   categoryAxisLineChart.getCategories().clear();
                    //   linechart.setTitle("EVOLUTION");
                    //   categoryaxis.getCategories().clear();
                    //    piechart.setTitle("REPRESENTATION");
                    //   barchart.setTitle("CATEGORIES");
                    cbParameter.getItems().clear();
                    cbParameter.getItems().add("Année");

                    cbParameter.getItems().add("Mois");

                } else if (newValue.toString().equals("Operation-Acte")) {

                    //  piechart.getData().clear();
                    //    barchart.getData().clear();
                    //  linechart.getData().clear();
                    //  categoryAxisLineChart.getCategories().clear();
                    //   linechart.setTitle("EVOLUTION");
                    //   categoryaxis.getCategories().clear();
                    //  piechart.setTitle("REPRESENTATION");
                    //  barchart.setTitle("CATEGORIES");
                    //  cbParameter.getItems().clear();
                    //  cbParameter.getItems().add("Tout");
                    List<Acte> ActesList = operationStat.getActeList();

                    for (Acte acte : ActesList) {
                        cbParameter.getItems().add(acte.getDescription());
                    }

                }
            }
        });
    }

    public void initTrancheAge() {

        List<String> trancheAge = new ArrayList();
        trancheAge.add("0-18");
        trancheAge.add(">18");

    }

    private void patientSexeHandler(Object newValue) throws IOException {

        PatientBySexe[] listNumberOfPatientBySexe = null;

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Patient-Sexe")) {
            param_pie_or_bar = newValue.toString();
            barchart_status = 1;
            piechart_status = 1;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/patient-sexe", "sexe=" + newValue.toString().trim());
            response.read();
            listNumberOfPatientBySexe
                    = new Gson().fromJson(response.getResponse(), PatientBySexe[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    Arrays.asList("Femme", "Homme")));

            for (PatientBySexe patient_sexe : listNumberOfPatientBySexe) {

                pieChartData.add(new PieChart.Data(patient_sexe.getSexe(), patient_sexe.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(patient_sexe.getSexe());

                dataSeries1.getData().add(new XYChart.Data(patient_sexe.getSexe(), patient_sexe.getNumber_of_operation()));

                barchart.getData().add(dataSeries1);

            }

            barchart.setTitle("Nombre de Patient par sexe");

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre de Patient par sexe");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );

            /*  piechart.getData().forEach(
                    (PieChart.Data data)
                    -> {
                
                Tooltip tooltip = new Tooltip(data.pieValueProperty().getValue() + " "+data.getName());
                Tooltip.install(data.getNode(), tooltip);
            }
            );*/
        }

    }

    private void patientSexeHandler(String newValue, BarChart b, PieChart p) throws IOException {

        PatientBySexe[] listNumberOfPatientBySexe = null;

        if (!newValue.trim().equals("")) {
            // listNumberOfPatientBySexe = patientstat.getNumberOfPatientBySexe(newValue.toString());
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/patient-sexe", "sexe=" + newValue.toString().trim());
            response.read();
            listNumberOfPatientBySexe
                    = new Gson().fromJson(response.getResponse(), PatientBySexe[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            ObservableList<PatientBySexe> patient_by_sexe = FXCollections.observableArrayList();
            b.getData().clear();
            // categoryaxis.getCategories().clear();
            //  categoryaxis.setCategories(FXCollections.<String>observableArrayList(
            //    Arrays.asList("Femme", "Homme")));

            for (PatientBySexe patient_sexe : listNumberOfPatientBySexe) {

                pieChartData.add(new PieChart.Data(patient_sexe.getSexe(), patient_sexe.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(patient_sexe.getSexe());

                dataSeries1.getData().add(new XYChart.Data(patient_sexe.getSexe(), patient_sexe.getNumber_of_operation()));

                b.getData().add(dataSeries1);

                patient_by_sexe.add(new PatientBySexe(patient_sexe.getSexe(), patient_sexe.getNumber_of_operation()));

            }

            /*   for (int i = 0; i < 100; i++) {
                patient_by_sexe.add(new PatientBySexe("Homme", 13));

            }*/
            for (PatientBySexe pat : patient_by_sexe) {
                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientSexeHandler()" + pat.getSexe() + " " + pat.getNumber_of_operation());

            }

            tableView1.setItems(patient_by_sexe);

            b.setTitle("Nombre de Patient par sexe");

            p.setData(pieChartData);

            p.setTitle("Nombre de Patient par sexe");

            p.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );

            /*  piechart.getData().forEach(
                    (PieChart.Data data)
                    -> {
                
                Tooltip tooltip = new Tooltip(data.pieValueProperty().getValue() + " "+data.getName());
                Tooltip.install(data.getNode(), tooltip);
            }
            );*/
        }

    }

    private void patientAgeRangeHandler(Object newValue) throws IOException {

        //  List<Object[]> patientAgeRangeHandler = null;
        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Patient-DateDeNaissance")) {
            this.barchart_status = 2;
            this.piechart_status = 2;
            param_pie_or_bar = newValue.toString();
            PatientByAgeRange[] patientAgeRangeHandler = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            String param = "";
            String regexp = ">(\\d+)";
            if (newValue.toString().matches(regexp)) {

                param = newValue.toString().trim().replaceAll("\\s+", "").split(">")[1];

            } else {
                param = newValue.toString();
            }
            HttpResponse response;
            if (param.toString().toLowerCase().equals("tout")) {

                String s = "";

                for (int i = 0; i < this.trancheAge.size(); i++) {
                    if (i == this.trancheAge.size() - 1) {
                        s = s.concat(this.trancheAge.get(i));
                    } else {
                        String s1 = this.trancheAge.get(i) + ";";
                        s = s.concat(s1);
                    }
                }

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()" + s);

                response = http.get("/patient-sexe", "age=" + param + "&age_range=" + s);
            } else {
                response = http.get("/patient-sexe", "age=" + param);
            }
            response.read();
            patientAgeRangeHandler
                    = new Gson().fromJson(response.getResponse(), PatientByAgeRange[].class
                    );
            //   patientAgeRangeHandler = patientstat.getNumberOfPatientByAgeRange(newValue.toString());

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    Arrays.asList("0-18", ">18")));
            piechart.getData().clear();
            for (PatientByAgeRange patientAgeRange : patientAgeRangeHandler) {
                /* System.out.println("p1: " + patientAgeRange[0]);
                System.out.println("p2: " + patientAgeRange[1]);*/
                pieChartData.add(new PieChart.Data(patientAgeRange.getAgeRange(), patientAgeRange.getNumber_of_patient()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(patientAgeRange.getAgeRange());

                dataSeries1.getData().add(new XYChart.Data(patientAgeRange.getAgeRange(), patientAgeRange.getNumber_of_patient()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                barchart.getData().add(dataSeries1);

            }

            barchart.setTitle("Nombre de Patient par Tranche d'age");

            barchart.setBarGap(3);
            barchart.setCategoryGap(20);

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre de Patient par Tranche d'age");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );

        }
    }

    private void patientAgeRangeHandler(String newValue, BarChart b, PieChart p) throws IOException {

        //  List<Object[]> patientAgeRangeHandler = null;
        if (!newValue.trim().equals("")) {

            PatientByAgeRange[] patientAgeRangeHandler = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            String param = "";
            String regexp = ">(\\d+)";
            if (newValue.toString().matches(regexp)) {

                param = newValue.toString().trim().replaceAll("\\s+", "").split(">")[1];

            } else {
                param = newValue.toString();
            }
            HttpResponse response;

            if (param.toString().toLowerCase().equals("tout")) {

                String s = "";

                this.initTrancheAge();
                for (int i = 0; i < this.trancheAge.size(); i++) {
                    if (i == this.trancheAge.size() - 1) {
                        s = s.concat(this.trancheAge.get(i));
                    } else {
                        String s1 = this.trancheAge.get(i) + ";";
                        s = s.concat(s1);
                    }
                }
                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()" + s);
                response = http.get("/patient-sexe", "age=" + param + "&age_range=" + s);
            } else {
                response = http.get("/patient-sexe", "age=" + param);
            }
            response.read();
            patientAgeRangeHandler
                    = new Gson().fromJson(response.getResponse(), PatientByAgeRange[].class
                    );
            //   patientAgeRangeHandler = patientstat.getNumberOfPatientByAgeRange(newValue.toString());

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            b.getData().clear();
            //  categoryaxis.getCategories().clear();
            // categoryaxis.setCategories(FXCollections.<String>observableArrayList(
            //     Arrays.asList("0-18", ">18")));
            p.getData().clear();

            /*  ObservableList<PatientBySexe> patient_by_sexe = FXCollections.observableArrayList();
        patient_by_sexe.add(new PatientBySexe("Homme", 10));
      
        tableView1.setItems(patient_by_sexe);*/
            ObservableList<PatientByAgeRange> patient_by_age = FXCollections.observableArrayList();

            for (PatientByAgeRange patientAgeRange : patientAgeRangeHandler) {
                /* System.out.println("p1: " + patientAgeRange[0]);
                System.out.println("p2: " + patientAgeRange[1]);*/
                pieChartData.add(new PieChart.Data(patientAgeRange.getAgeRange(), patientAgeRange.getNumber_of_patient()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(patientAgeRange.getAgeRange());

                dataSeries1.getData().add(new XYChart.Data(patientAgeRange.getAgeRange(), patientAgeRange.getNumber_of_patient()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                b.getData().add(dataSeries1);
                patient_by_age.add(new PatientByAgeRange(patientAgeRange.getAgeRange(),
                        patientAgeRange.getNumber_of_patient()));

            }
            tableView2.setItems(patient_by_age);

            b.setTitle("Nombre de Patient par Tranche d'age");

            b.setBarGap(3);
            b.setCategoryGap(20);

            p.setData(pieChartData);

            p.setTitle("Nombre de Patient par Tranche d'age");

            p.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );

        }
    }

    public void operationByMedecin(Object newValue) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Medecin")) {

            this.barchart_status = 3;
            this.piechart_status = 3;
            param_pie_or_bar = newValue.toString();
            /*List<Object[]> ListNumberOfOperationByMedecin = null;
            ListNumberOfOperationByMedecin = operationStat.getNumberOfOperationByMedecin(newValue.toString());*/
            OperationByMedecin[] ListNumberOfOperationByMedecin = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/operation", "medecin=" + newValue.toString().trim());
            response.read();
            ListNumberOfOperationByMedecin
                    = new Gson().fromJson(response.getResponse(), OperationByMedecin[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            piechart.getData().clear();

            for (OperationByMedecin operation_medecin : ListNumberOfOperationByMedecin) {

                categoriesList.add(operation_medecin.getNom() + " " + operation_medecin.getPrénom());
                pieChartData.add(new PieChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_medecin.getNom() + " " + operation_medecin.getPrénom());

                dataSeries1.getData().add(new XYChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                barchart.getData().add(dataSeries1);

            }

            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));

            barchart.setTitle("Nombre d'operation  par Medecin");

            barchart.setBarGap(3);
            barchart.setCategoryGap(20);

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre d'operation  par Medecin");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }

    }

    public void operationByMedecin(Object newValue, LocalDate start, LocalDate end) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Medecin")) {
            barchart_status = 3;
            piechart_status = 3;
            param_pie_or_bar = newValue.toString();
            /* List<Object[]> ListNumberOfOperationByMedecin = null;
            ListNumberOfOperationByMedecin = operationStat.getNumberOfOperationByMedecin(newValue.toString(), start, end);*/
            OperationByMedecin[] ListNumberOfOperationByMedecin = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/operation", "medecin=" + newValue.toString().trim() + "&start=" + start + "&end=" + end);
            response.read();
            ListNumberOfOperationByMedecin
                    = new Gson().fromJson(response.getResponse(), OperationByMedecin[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            piechart.getData().clear();

            for (OperationByMedecin operation_medecin : ListNumberOfOperationByMedecin) {

                categoriesList.add(operation_medecin.getNom() + " " + operation_medecin.getPrénom());
                pieChartData.add(new PieChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_medecin.getNom() + " " + operation_medecin.getPrénom());

                dataSeries1.getData().add(new XYChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                barchart.getData().add(dataSeries1);

            }

            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));

            barchart.setTitle("Nombre d'operation  par Medecin");

            barchart.setBarGap(3);
            barchart.setCategoryGap(20);

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre d'operation  par Medecin");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }

    }

    public void operationByMedecin(String newValue, LocalDate start, LocalDate end, BarChart b, PieChart p) throws IOException {

        if (!newValue.trim().equals("")) {

            /* List<Object[]> ListNumberOfOperationByMedecin = null;
            ListNumberOfOperationByMedecin = operationStat.getNumberOfOperationByMedecin(newValue.toString(), start, end);*/
            OperationByMedecin[] ListNumberOfOperationByMedecin = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response;
            if (start == null) {
                response = http.get("/operation", "medecin=" + newValue.toString().trim());
            } else {
                response = http.get("/operation", "medecin=" + newValue.toString().trim() + "&start=" + start + "&end=" + end);
            }
            response.read();
            ListNumberOfOperationByMedecin
                    = new Gson().fromJson(response.getResponse(), OperationByMedecin[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            b.getData().clear();
            // categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            p.getData().clear();

            /*  ObservableList<PatientBySexe> patient_by_sexe = FXCollections.observableArrayList();
        patient_by_sexe.add(new PatientBySexe("Homme", 10));
      
        tableView1.setItems(patient_by_sexe);*/
            ObservableList<OperationByMedecin> operation_by_medecin = FXCollections.observableArrayList();

            for (OperationByMedecin operation_medecin : ListNumberOfOperationByMedecin) {

                categoriesList.add(operation_medecin.getNom() + " " + operation_medecin.getPrénom());
                pieChartData.add(new PieChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_medecin.getNom() + " " + operation_medecin.getPrénom());

                dataSeries1.getData().add(new XYChart.Data(operation_medecin.getNom() + " " + operation_medecin.getPrénom(), operation_medecin.getNumber_of_operation()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                b.getData().add(dataSeries1);
                operation_by_medecin.add(new OperationByMedecin(operation_medecin.getNom(),
                        operation_medecin.getPrénom(),
                        operation_medecin.getNumber_of_operation()
                ));
            }

            tableView3.setItems(operation_by_medecin);

            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));

            b.setTitle("Nombre d'operation  par Medecin");

            b.setBarGap(3);
            b.setCategoryGap(20);

            p.setData(pieChartData);

            p.setTitle("Nombre d'operation  par Medecin");

            p.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }

    }

    public void operationByPeriod(Object newValue) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Date operation")) {
            this.linechart_status = 1;
            param_line = newValue.toString();
            if (newValue.toString().trim().equals("Année")) {
                OperationByAnnée[] ListNumberOfOperationByYear;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response = http.get("/operation", "date=year");
                response.read();
                System.out.println("com.ibnhamza.ihsaai.controller.MainController.operationByPeriod()" + response.getResponse());
                ListNumberOfOperationByYear
                        = new Gson().fromJson(response.getResponse(), OperationByAnnée[].class
                        );

                linechart.getData().clear();
                categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                // System.out.println("   Année"+ListNumberOfOperationByYear.length);
                for (OperationByAnnée operation_by_year : ListNumberOfOperationByYear) {

                    System.err.println(" " + operation_by_year.getAnnée());

                    categoriesList.add(Integer.toString(operation_by_year.getAnnée()));

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(Integer.toString(operation_by_year.getAnnée()), operation_by_year.getNombre_operation()));

                }
                categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));
                linechart.getData().addAll(dataSeries1);
                linechart.setTitle("Nombre d'operation  par Periode");

            } else if (newValue.toString().trim().equals("Mois")) {
                OperationByMonth[] ListNumberOfOperationByMonth = null;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response = http.get("/operation", "date=mois");
                response.read();
                ListNumberOfOperationByMonth
                        = new Gson().fromJson(response.getResponse(), OperationByMonth[].class
                        );

                linechart.getData().clear();
                categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                for (OperationByMonth operation_by_month : ListNumberOfOperationByMonth) {
                    categoriesList.add(operation_by_month.getMois() + " " + operation_by_month.getAnnée());

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(operation_by_month.getMois() + " " + operation_by_month.getAnnée(), operation_by_month.getNombre_operation()));

                }

                categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));
                linechart.getData().addAll(dataSeries1);
                linechart.setTitle("Nombre d'operation  par Periode");

            }

        }

    }

    public void operationByPeriod(Object newValue, LocalDate start, LocalDate end) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Date operation")) {
            this.linechart_status = 1;
            param_line = newValue.toString();
            if (newValue.toString().trim().equals("Année")) {
                OperationByAnnée[] ListNumberOfOperationByYear;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response = http.get("/operation", "date=year&start=" + start + "&end=" + end);
                response.read();
                System.out.println("com.ibnhamza.ihsaai.controller.MainController.operationByPeriod()" + response.getResponse());
                ListNumberOfOperationByYear
                        = new Gson().fromJson(response.getResponse(), OperationByAnnée[].class
                        );

                linechart.getData().clear();
                categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                // System.out.println("   Année"+ListNumberOfOperationByYear.length);
                for (OperationByAnnée operation_by_year : ListNumberOfOperationByYear) {

                    System.err.println(" " + operation_by_year.getAnnée());

                    categoriesList.add(Integer.toString(operation_by_year.getAnnée()));

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(Integer.toString(operation_by_year.getAnnée()), operation_by_year.getNombre_operation()));

                }
                categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));
                linechart.getData().addAll(dataSeries1);
                linechart.setTitle("Nombre d'operation  par Periode");

            } else if (newValue.toString().trim().equals("Mois")) {
                OperationByMonth[] ListNumberOfOperationByMonth = null;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response = http.get("/operation", "date=mois&start=" + start + "&end=" + end);
                response.read();
                ListNumberOfOperationByMonth
                        = new Gson().fromJson(response.getResponse(), OperationByMonth[].class
                        );

                linechart.getData().clear();
                categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                for (OperationByMonth operation_by_month : ListNumberOfOperationByMonth) {
                    categoriesList.add(operation_by_month.getMois() + " " + operation_by_month.getAnnée());

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(operation_by_month.getMois() + " " + operation_by_month.getAnnée(), operation_by_month.getNombre_operation()));

                }

                categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));
                linechart.getData().addAll(dataSeries1);
                linechart.setTitle("Nombre d'operation  par Periode");

            }

        }

    }

    public void operationByPeriod(String newValue, LocalDate start, LocalDate end, LineChart l) throws IOException {

        if (!newValue.trim().equals("")) {

            if (newValue.trim().equals("Année")) {
                OperationByAnnée[] ListNumberOfOperationByYear;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response;
                if (start == null) {
                    response = http.get("/operation", "date=year");

                } else {
                    response = http.get("/operation", "date=year&start=" + start + "&end=" + end);

                }

                response.read();
                System.out.println("com.ibnhamza.ihsaai.controller.MainController.operationByPeriod()" + response.getResponse());
                ListNumberOfOperationByYear
                        = new Gson().fromJson(response.getResponse(), OperationByAnnée[].class
                        );

                l.getData().clear();
                //  categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                ObservableList<OperationByAnnée> operation_bu_year = FXCollections.observableArrayList();

                for (OperationByAnnée operation_by_year : ListNumberOfOperationByYear) {

                    System.err.println(" " + operation_by_year.getAnnée());

                    categoriesList.add(Integer.toString(operation_by_year.getAnnée()));

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(Integer.toString(operation_by_year.getAnnée()), operation_by_year.getNombre_operation()));

                    operation_bu_year.add(new OperationByAnnée(
                            operation_by_year.getAnnée(),
                            operation_by_year.getNombre_operation()
                    ));
                }
                tableView41.setItems(operation_bu_year);
                /*    categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));*/
                l.getData().addAll(dataSeries1);
                l.setTitle("Nombre d'operation  par Periode");

            } else if (newValue.trim().equals("Mois")) {
                OperationByMonth[] ListNumberOfOperationByMonth = null;
                HttpClient.setPort("8888");
                HttpClient http = new HttpClient();
                HttpResponse response;
                if (start == null) {
                    response = http.get("/operation", "date=mois");

                } else {
                    response = http.get("/operation", "date=mois&start=" + start + "&end=" + end);

                }

                response.read();
                ListNumberOfOperationByMonth
                        = new Gson().fromJson(response.getResponse(), OperationByMonth[].class
                        );

                l.getData().clear();
                //  categoryAxisLineChart.getCategories().clear();
                List<String> categoriesList = new ArrayList();
                XYChart.Series dataSeries1 = new XYChart.Series();

                ObservableList<OperationByMonth> operation_month = FXCollections.observableArrayList();
                // operation_by_month.add(new PatientBySexe("Homme", 10));

                for (OperationByMonth operation_by_month : ListNumberOfOperationByMonth) {
                    categoriesList.add(operation_by_month.getMois() + " " + operation_by_month.getAnnée());

                    dataSeries1.setName("Nombre d'operation  par Periode");

                    dataSeries1.getData().add(new XYChart.Data(operation_by_month.getMois() + " " + operation_by_month.getAnnée(), operation_by_month.getNombre_operation()));

                    operation_month.add(new OperationByMonth(
                            operation_by_month.getAnnée(),
                            operation_by_month.getMois(),
                            operation_by_month.getNombre_operation()
                    ));
                }

                tableView42.setItems(operation_month);

                /*  categoryAxisLineChart.setCategories(FXCollections.<String>observableArrayList(
                        categoriesList));*/
                l.getData().addAll(dataSeries1);
                l.setTitle("Nombre d'operation  par Periode");

            }

        }

    }

    public void operationByConsultation(Object newValue) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Acte")) {
            this.barchart_status = 4;
            this.piechart_status = 4;
            param_pie_or_bar = newValue.toString();
            OperationByActe[] ListNumberOfOperationsByConsultation = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/operation", "acte=" + newValue.toString().trim());
            response.read();
            ListNumberOfOperationsByConsultation
                    = new Gson().fromJson(response.getResponse(), OperationByActe[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            piechart.getData().clear();

            for (OperationByActe operation_acte : ListNumberOfOperationsByConsultation) {

                categoriesList.add(operation_acte.getDescription());
                pieChartData.add(new PieChart.Data(operation_acte.getDescription(), operation_acte.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_acte.getDescription());

                dataSeries1.getData().add(new XYChart.Data(operation_acte.getDescription(),
                        operation_acte.getNumber_of_operation()));

                barchart.getData().add(dataSeries1);

            }

            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));

            barchart.setTitle("Nombre d'operation  par Acte");

            barchart.setBarGap(3);
            barchart.setCategoryGap(20);

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre d'operation  par Acte");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }
    }

    public void operationByConsultation(Object newValue, LocalDate start, LocalDate end) throws IOException {

        if (newValue != null && !newValue.toString().trim().equals("") && cbTable.getSelectionModel().getSelectedItem().toString().equals("Operation-Acte")) {
            this.barchart_status = 4;
            this.piechart_status = 4;
            param_pie_or_bar = newValue.toString();

            OperationByActe[] ListNumberOfOperationsByConsultation = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response = http.get("/operation", "acte=" + newValue.toString().trim() + "&start=" + start + "&end=" + end);
            response.read();
            ListNumberOfOperationsByConsultation
                    = new Gson().fromJson(response.getResponse(), OperationByActe[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            piechart.getData().clear();

            for (OperationByActe operation_acte : ListNumberOfOperationsByConsultation) {

                categoriesList.add(operation_acte.getDescription());
                pieChartData.add(new PieChart.Data(operation_acte.getDescription(), operation_acte.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_acte.getDescription());

                dataSeries1.getData().add(new XYChart.Data(operation_acte.getDescription(),
                        operation_acte.getNumber_of_operation()));

                barchart.getData().add(dataSeries1);

            }

            categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));

            barchart.setTitle("Nombre d'operation  par Acte");

            barchart.setBarGap(3);
            barchart.setCategoryGap(20);

            piechart.setData(pieChartData);

            piechart.setTitle("Nombre d'operation  par Acte");

            piechart.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }
    }

    public void operationByConsultation(String newValue, LocalDate start, LocalDate end, BarChart b, PieChart p) throws IOException {

        if (!newValue.trim().equals("")) {

            OperationByActe[] ListNumberOfOperationsByConsultation = null;
            HttpClient.setPort("8888");
            HttpClient http = new HttpClient();
            HttpResponse response;
            if (start == null) {
                response = http.get("/operation", "acte=" + newValue.trim());

            } else {
                response = http.get("/operation", "acte=" + newValue.trim() + "&start=" + start + "&end=" + end);

            }

            response.read();

            ListNumberOfOperationsByConsultation
                    = new Gson().fromJson(response.getResponse(), OperationByActe[].class
                    );

            ObservableList<PieChart.Data> pieChartData
                    = FXCollections.observableArrayList();

            barchart.getData().clear();
            // categoryaxis.getCategories().clear();
            List<String> categoriesList = new ArrayList();

            piechart.getData().clear();

            ObservableList<OperationByActe> operation_by_acte = FXCollections.observableArrayList();

            for (OperationByActe operation_acte : ListNumberOfOperationsByConsultation) {

                categoriesList.add(operation_acte.getDescription());
                pieChartData.add(new PieChart.Data(operation_acte.getDescription(), operation_acte.getNumber_of_operation()));

                XYChart.Series dataSeries1 = new XYChart.Series();
                dataSeries1.setName(operation_acte.getDescription());

                dataSeries1.getData().add(new XYChart.Data(operation_acte.getDescription(), operation_acte.getNumber_of_operation()));

                System.out.println("com.ibnhamza.ihsaai.controller.MainController.patientAgeRangeHandler()");
                b.getData().add(dataSeries1);
                operation_by_acte.add(new OperationByActe(operation_acte.getDescription(),
                        operation_acte.getNumber_of_operation()
                ));

            }
            tableView5.setItems(operation_by_acte);

            /*  categoryaxis.setCategories(FXCollections.<String>observableArrayList(
                    categoriesList));*/
            b.setTitle("Nombre d'operation  par Acte");

            b.setBarGap(3);
            b.setCategoryGap(20);

            p.setData(pieChartData);

            p.setTitle("Nombre d'operation  par Acte");

            p.getData().forEach(
                    data
                    -> data.nameProperty().bind(
                            Bindings.concat(
                                    data.getName(), " ", data.pieValueProperty()
                            )
                    )
            );
        }
    }
}
