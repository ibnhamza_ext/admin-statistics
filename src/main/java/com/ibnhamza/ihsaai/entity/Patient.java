/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "patient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Patient.findAll", query = "SELECT p FROM Patient p")
    , @NamedQuery(name = "Patient.findById", query = "SELECT p FROM Patient p WHERE p.id = :id")
    , @NamedQuery(name = "Patient.findByAdresse", query = "SELECT p FROM Patient p WHERE p.adresse = :adresse")
    , @NamedQuery(name = "Patient.findByAyantDroit", query = "SELECT p FROM Patient p WHERE p.ayantDroit = :ayantDroit")
    , @NamedQuery(name = "Patient.findByChronique", query = "SELECT p FROM Patient p WHERE p.chronique = :chronique")
    , @NamedQuery(name = "Patient.findByCp", query = "SELECT p FROM Patient p WHERE p.cp = :cp")
    , @NamedQuery(name = "Patient.findByDateCreation", query = "SELECT p FROM Patient p WHERE p.dateCreation = :dateCreation")
    , @NamedQuery(name = "Patient.findByDateNaissance", query = "SELECT p FROM Patient p WHERE p.dateNaissance = :dateNaissance")
    , @NamedQuery(name = "Patient.findByGroupage", query = "SELECT p FROM Patient p WHERE p.groupage = :groupage")
    , @NamedQuery(name = "Patient.findByLieuNaissance", query = "SELECT p FROM Patient p WHERE p.lieuNaissance = :lieuNaissance")
    , @NamedQuery(name = "Patient.findByNom", query = "SELECT p FROM Patient p WHERE p.nom = :nom")
    , @NamedQuery(name = "Patient.findByNomArabe", query = "SELECT p FROM Patient p WHERE p.nomArabe = :nomArabe")
    , @NamedQuery(name = "Patient.findByNomJeuneFille", query = "SELECT p FROM Patient p WHERE p.nomJeuneFille = :nomJeuneFille")
    , @NamedQuery(name = "Patient.findByNumeroCarte", query = "SELECT p FROM Patient p WHERE p.numeroCarte = :numeroCarte")
    , @NamedQuery(name = "Patient.findByNss", query = "SELECT p FROM Patient p WHERE p.nss = :nss")
    , @NamedQuery(name = "Patient.findByPhoto", query = "SELECT p FROM Patient p WHERE p.photo = :photo")
    , @NamedQuery(name = "Patient.findByPrenom", query = "SELECT p FROM Patient p WHERE p.prenom = :prenom")
    , @NamedQuery(name = "Patient.findByPrenomArabe", query = "SELECT p FROM Patient p WHERE p.prenomArabe = :prenomArabe")
    , @NamedQuery(name = "Patient.findByProfession", query = "SELECT p FROM Patient p WHERE p.profession = :profession")
    , @NamedQuery(name = "Patient.findBySexe", query = "SELECT p FROM Patient p WHERE p.sexe = :sexe")
    , @NamedQuery(name = "Patient.findBySituationFamilliale", query = "SELECT p FROM Patient p WHERE p.situationFamilliale = :situationFamilliale")
    , @NamedQuery(name = "Patient.findBySolde", query = "SELECT p FROM Patient p WHERE p.solde = :solde")
    , @NamedQuery(name = "Patient.findByTauxReduction", query = "SELECT p FROM Patient p WHERE p.tauxReduction = :tauxReduction")
    , @NamedQuery(name = "Patient.findByTelephone", query = "SELECT p FROM Patient p WHERE p.telephone = :telephone")
    , @NamedQuery(name = "Patient.findByTitre", query = "SELECT p FROM Patient p WHERE p.titre = :titre")
    , @NamedQuery(name = "Patient.findByTypeConvention", query = "SELECT p FROM Patient p WHERE p.typeConvention = :typeConvention")
    , @NamedQuery(name = "Patient.findByAntecedentFamilliaux", query = "SELECT p FROM Patient p WHERE p.antecedentFamilliaux = :antecedentFamilliaux")
    , @NamedQuery(name = "Patient.findByAntecedentPerso", query = "SELECT p FROM Patient p WHERE p.antecedentPerso = :antecedentPerso")
    , @NamedQuery(name = "Patient.findByReduction", query = "SELECT p FROM Patient p WHERE p.reduction = :reduction")})
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "adresse")
    private String adresse;
    @Column(name = "ayantDroit")
    private Boolean ayantDroit;
    @Column(name = "chronique")
    private Boolean chronique;
    @Column(name = "cp")
    private String cp;
    @Column(name = "date_creation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;
    @Column(name = "groupage")
    private String groupage;
    @Column(name = "lieu_naissance")
    private String lieuNaissance;
    @Column(name = "nom")
    private String nom;
    @Column(name = "nom_arabe")
    private String nomArabe;
    @Column(name = "nom_jeune_fille")
    private String nomJeuneFille;
    @Column(name = "numero_carte")
    private String numeroCarte;
    @Column(name = "nss")
    private String nss;
    @Lob
    @Column(name = "observation")
    private String observation;
    @Column(name = "photo")
    private String photo;
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "prenom_arabe")
    private String prenomArabe;
    @Column(name = "profession")
    private String profession;
    @Column(name = "sexe")
    private String sexe;
    @Column(name = "situation_familliale")
    private String situationFamilliale;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "solde")
    private Double solde;
    @Column(name = "taux_reduction")
    private Double tauxReduction;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "titre")
    private String titre;
    @Lob
    @Column(name = "traitement_en_cours")
    private String traitementEnCours;
    @Column(name = "type_convention")
    private String typeConvention;
    @Column(name = "antecedent_familliaux")
    private String antecedentFamilliaux;
    @Column(name = "antecedent_perso")
    private String antecedentPerso;
    @Column(name = "reduction")
    private Float reduction;
    @OneToMany(mappedBy = "patient")
    private Collection<Consultation> consultationCollection;
    @OneToMany(mappedBy = "patientId")
    private Collection<Operation> operationCollection;

    public Patient() {
    }

    public Patient(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Boolean getAyantDroit() {
        return ayantDroit;
    }

    public void setAyantDroit(Boolean ayantDroit) {
        this.ayantDroit = ayantDroit;
    }

    public Boolean getChronique() {
        return chronique;
    }

    public void setChronique(Boolean chronique) {
        this.chronique = chronique;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getGroupage() {
        return groupage;
    }

    public void setGroupage(String groupage) {
        this.groupage = groupage;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomArabe() {
        return nomArabe;
    }

    public void setNomArabe(String nomArabe) {
        this.nomArabe = nomArabe;
    }

    public String getNomJeuneFille() {
        return nomJeuneFille;
    }

    public void setNomJeuneFille(String nomJeuneFille) {
        this.nomJeuneFille = nomJeuneFille;
    }

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPrenomArabe() {
        return prenomArabe;
    }

    public void setPrenomArabe(String prenomArabe) {
        this.prenomArabe = prenomArabe;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getSituationFamilliale() {
        return situationFamilliale;
    }

    public void setSituationFamilliale(String situationFamilliale) {
        this.situationFamilliale = situationFamilliale;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Double getTauxReduction() {
        return tauxReduction;
    }

    public void setTauxReduction(Double tauxReduction) {
        this.tauxReduction = tauxReduction;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTraitementEnCours() {
        return traitementEnCours;
    }

    public void setTraitementEnCours(String traitementEnCours) {
        this.traitementEnCours = traitementEnCours;
    }

    public String getTypeConvention() {
        return typeConvention;
    }

    public void setTypeConvention(String typeConvention) {
        this.typeConvention = typeConvention;
    }

    public String getAntecedentFamilliaux() {
        return antecedentFamilliaux;
    }

    public void setAntecedentFamilliaux(String antecedentFamilliaux) {
        this.antecedentFamilliaux = antecedentFamilliaux;
    }

    public String getAntecedentPerso() {
        return antecedentPerso;
    }

    public void setAntecedentPerso(String antecedentPerso) {
        this.antecedentPerso = antecedentPerso;
    }

    public Float getReduction() {
        return reduction;
    }

    public void setReduction(Float reduction) {
        this.reduction = reduction;
    }

    @XmlTransient
    public Collection<Consultation> getConsultationCollection() {
        return consultationCollection;
    }

    public void setConsultationCollection(Collection<Consultation> consultationCollection) {
        this.consultationCollection = consultationCollection;
    }

    @XmlTransient
    public Collection<Operation> getOperationCollection() {
        return operationCollection;
    }

    public void setOperationCollection(Collection<Operation> operationCollection) {
        this.operationCollection = operationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.Patient[ id=" + id + " ]";
    }
    
}
