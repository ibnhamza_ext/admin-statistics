/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import Api.Operation;
import Api.Patient;
import Api.firstServlet;
import ibnhamza.rest.api.HttpClient;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import static io.undertow.servlet.Servlets.defaultContainer;
import static io.undertow.servlet.Servlets.deployment;
import static io.undertow.servlet.Servlets.servlet;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ListenerInfo;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ROGUE1
 */
public class ServletEngine {

    public static final String MYAPP = "/myapp";

    public static void main(final String[] args) {
        try {
            DeploymentInfo servletBuilder = Servlets.deployment().setClassLoader(ServletEngine.class.getClassLoader())
                    .setDeploymentName("myapp").setContextPath("/api")
                    .addServlets(Servlets.servlet("patient-sexe",
                            new Patient().getClass()).addMapping("/patient-sexe"))
                     .addServlets(Servlets.servlet("operation",
                            new Operation().getClass()).addMapping("/operation"));
                   
            DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
            manager.deploy();
            PathHandler path = Handlers.path(Handlers.redirect("/api")).addPrefixPath("/api", manager.start());
            Undertow server = Undertow.builder().addHttpListener(8888, "localhost").setHandler(path).build();
            server.start();
        } catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
