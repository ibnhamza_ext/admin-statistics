/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate;

import com.ibnhamza.ihsaai.entity.Acte;
import com.ibnhamza.ihsaai.entity.Consultation;
import com.ibnhamza.ihsaai.entity.DetailOperation;
import com.ibnhamza.ihsaai.entity.Fonctionnaire;
import com.ibnhamza.ihsaai.entity.Operation;
import com.ibnhamza.ihsaai.entity.Patient;
import java.util.List;
import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author ROGUE1
 */
public class HibernateUtil {

    static Session sessionObj;
    static SessionFactory sessionFactoryObj;

    // This Method Is Used To Create The Hibernate's SessionFactory Object
    public static SessionFactory buildSessionFactory() {

        Configuration configObj = new Configuration();
        configObj.configure("hibernate.cfg.xml");

        configObj.addAnnotatedClass(Acte.class);
        configObj.addAnnotatedClass(DetailOperation.class);
        configObj.addAnnotatedClass(Consultation.class);
        configObj.addAnnotatedClass(Fonctionnaire.class);
        configObj.addAnnotatedClass(Operation.class);
        configObj.addAnnotatedClass(Patient.class);

        // Since Hibernate Version 4.x, ServiceRegistry Is Being Used
        ServiceRegistry serviceRegistryObj = new StandardServiceRegistryBuilder().applySettings(configObj.getProperties()).build();

        // Creating Hibernate SessionFactory Instance
        sessionFactoryObj = configObj.buildSessionFactory(serviceRegistryObj);
        return sessionFactoryObj;

    }
    public static void displayRecords() {

        int emp_id;
        Acte empObj;
        Acte empObj2;
        try {
            sessionObj = buildSessionFactory().openSession();

            // Get The Employee Details Whose Emp_Id is 1
            emp_id = 1;
            empObj = (Acte) sessionObj.get(Acte.class, new Integer(emp_id));
            List<Acte> Acte = sessionObj.createQuery("from Acte", Acte.class).getResultList();

            for (int i = 0; i < Acte.size(); i++) {
                System.out.println("\nEmployee Record?= " + Acte.get(i).getDescription());
            }

            if (empObj != null) {
                System.out.println("\nEmployee Record?= " + empObj.getDescription());
            }

        } catch (Exception sqlException) {
            /*  if(null != sessionObj.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                sessionObj.getTransaction().rollback();
            }*/
            sqlException.printStackTrace();
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
        }

    }

}
