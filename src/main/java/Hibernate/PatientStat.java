/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate;

import static Hibernate.HibernateUtil.sessionObj;
import com.ibnhamza.ihsaai.entity.Acte;
import com.ibnhamza.ihsaai.entity.Patient;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author ROGUE1
 */
public class PatientStat {

    /**
     *
     */
    public Session sessionObj;

    public PatientStat() {
        sessionObj = HibernateUtil.buildSessionFactory().openSession();
    }

    public List<Object[]> getNumberOfPatientBySexe(String param) {
        List<Object[]> listNumberOfPatientBySexe = null;
        String query = null;
        if (param.trim().toLowerCase().equals("tout")) {
            query = "select case when p.sexe='g' then 'Homme' else 'Femme' end ,count(p) from Patient p group by p.sexe";
        } else if (param.trim().toLowerCase().equals("homme")) {
            query = "select case when p.sexe='g' then 'Homme' else 'Femme' end ,count(p) from Patient p where p.sexe='g'";
        } else if (param.trim().toLowerCase().equals("femme")) {
            query = "select case when p.sexe='g' then 'Homme' else 'Femme' end ,count(p) from Patient p where p.sexe!='g'";
        }

        try {
            /* sessionObj = HibernateUtil.buildSessionFactory().openSession();*/

            listNumberOfPatientBySexe = sessionObj.createQuery(query, Object[].class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listNumberOfPatientBySexe;
    }

    public List<Object> getTrancheAge() {
        List<Object> listTrancheAge = null;

        try {
            /* sessionObj = HibernateUtil.buildSessionFactory().openSession();*/

            listTrancheAge = sessionObj.createQuery("SELECT distinct(case when (year(CURRENT_DATE())-year(dateNaissance))>18 then '0-18' else '>18'  end)as age   FROM Patient ", Object.class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listTrancheAge;
    }

    public List<Object[]> getNumberOfPatientByAgeRangeApi(String param) {
        List<Object[]> listNumberOfPatientByAgeRange = null;
        String query = null;

        if (param.split("-").length == 2) {

            query = "SELECT count(id),"
                    + "case when  (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=:age_min and (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<=:age_max then concat(:age_min,concat('-',:age_max)) ELSE '' END  "
                    + "from Patient "
                    + "where   (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=:age_min and (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<=:age_max";

        } else if (param.split(">").length == 1) {
            query = "SELECT count(id),"
                    + "case when (year(CURRENT_DATE())-year(dateNaissance))>=18 then concat('>',:age_max) ELSE '' END  "
                    + "from Patient "
                    + "where  (year(CURRENT_DATE())-year(dateNaissance))>=:age_max";

        }

        try {
            /* sessionObj = HibernateUtil.buildSessionFactory().openSession();*/

            if (param.split("-").length == 2) {
                listNumberOfPatientByAgeRange = sessionObj.createQuery(query, Object[].class)
                        .setParameter("age_min", Integer.parseInt(param.split("-")[0]))
                        .setParameter("age_max", Integer.parseInt(param.split("-")[1]))
                        .getResultList();

            } else if (param.split(">").length == 1) {

                listNumberOfPatientByAgeRange = sessionObj.createQuery(query, Object[].class)
                        .setParameter("age_max", Integer.parseInt(param.split(">")[0]))
                        .getResultList();

            }

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listNumberOfPatientByAgeRange;

    }

    public List<Object[]> getNumberOfPatientByAgeRange(String param, String param1) {
        List<Object[]> listNumberOfPatientByAgeRange = null;
        String query = null;
        if (param.trim().toLowerCase().equals("tout")) {
            String age_range[] = param1.split(";");
             System.out.println("Hibernate.PatientStat.getNumberOfPatientByAgeRange()"+param1);
            String query1 = "";

            for (int i = 0; i < age_range.length; i++) {
                 String param2=age_range[i];
               
                if (param2.split("-").length == 2) {
                    
                    String query2="when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=" + param2.split("-")[0]
                            + " and (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<=" + param2.split("-")[1]
                            + " then concat("+param2.split("-")[0]+",concat('-',"+param2.split("-")[1]+"))";
                     query1= query1.concat(query2);
                   
                } else if (param.split(">").length == 1) {

                    String query2 = " when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=" + param2.split(">")[1]
                            + " then concat('>',"+param2.split(">")[1]+")";
                      query1= query1.concat(query2);
                   
                }

            }

            query1+="END";
            query = " SELECT count(id),"
                    + " case "
                    + query1
                    + " from Patient "
                    + " group by case "
                    + query1;

            try {
                /* sessionObj = HibernateUtil.buildSessionFactory().openSession();*/

                listNumberOfPatientByAgeRange = sessionObj.createQuery(query, Object[].class).getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
           }

            
            for(Object o[]:listNumberOfPatientByAgeRange){
                System.out.println("Hibernate.PatientStat.getNumberOfPatientByAgeRange()"+o[0]+" "+o[1]);
            }
            return listNumberOfPatientByAgeRange;

        }

        //  if(param.trim().toLowerCase().equals("tout"))
        /*  query = "SELECT count(id),"
                    + "case when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<18 then '0-18' ELSE '>18' END  "
                    + "from Patient "
                    + "group by case when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<18 then '0-18' ELSE '>18' END  ";

        } else if (param.trim().equals("0-18")) {
            query = "SELECT count(id),"
                    + "case when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<18 then '0-18' ELSE '>18' END  "
                    + "from Patient "
                    + "where  (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))<=18";

        } else if (param.trim().equals(">18")) {
            query = "SELECT count(id),"
                    + "case when (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=18 then '0-18' ELSE '>18' END  "
                    + "from Patient "
                    + "where  (floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(date_naissance, '%Y-%m-%d'))/365.25))>=18";

        }*/
        return null;
    }

}
