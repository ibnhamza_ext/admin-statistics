/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate;

import com.ibnhamza.ihsaai.entity.Acte;
import com.ibnhamza.ihsaai.entity.Fonctionnaire;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author ROGUE1
 */
public class OperationStat {

    public Session sessionObj;

    public OperationStat() {
        sessionObj = HibernateUtil.buildSessionFactory().openSession();
    }

    public List<Fonctionnaire> getMedcins() {
        List<Fonctionnaire> listMedecins = null;

        try {
            /* sessionObj = HibernateUtil.buildSessionFactory().openSession();*/

            listMedecins = sessionObj.createQuery("select distinct f "
                    + "FROM Operation o,Fonctionnaire f "
                    + "where f.id=o.medecinId ", Fonctionnaire.class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listMedecins;
    }

    public List<Object[]> getNumberOfOperationByMedecin(String param) {
        String query = "";

        List<Object[]> listtNumberOfOperationByMedecin = null;
        if (param.trim().toLowerCase().equals("tout")) {
            query = "SELECT f.nom,f.prenom,count(*) "
                    + "FROM Operation o,Fonctionnaire f "
                    + "where f.id=o.medecinId "
                    + "group by f.nom,f.prenom";

            try {

                listtNumberOfOperationByMedecin = sessionObj.createQuery(query, Object[].class).getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        } else {
            String medecin[] = param.split(" ");
            System.err.println("koko param " + medecin[0] + " " + medecin[1]);

            query = "SELECT f.nom,f.prenom,count(*) "
                    + "FROM Operation o,Fonctionnaire f "
                    + "where f.id=o.medecinId "
                    + "and  f.nom=:nom"
                    + " and f.prenom=:prenom";

            try {

                listtNumberOfOperationByMedecin = sessionObj.createQuery(query, Object[].class)
                        .setParameter("nom", medecin[0])
                        .setParameter("prenom", medecin[1])
                        .getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        }

        return listtNumberOfOperationByMedecin;

    }

    public List<Object[]> getNumberOfOperationByMedecin(String param, LocalDate start, LocalDate end) {
        String query = "";
        Date d1 = Date.from(start.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date d2 = Date.from(end.atStartOfDay(ZoneId.systemDefault()).toInstant());
        List<Object[]> listtNumberOfOperationByMedecin = null;
        if (param.trim().toLowerCase().equals("tout")) {
            query = "SELECT f.nom,f.prenom,count(*) "
                    + "FROM Operation o,Fonctionnaire f "
                    + "where f.id=o.medecinId "
                    + "and DATE(o.dateOperation)>=:start "
                    + "and DATE(o.dateOperation)<=:end "
                    + "group by f.nom,f.prenom";

            try {

                listtNumberOfOperationByMedecin = sessionObj.createQuery(query, Object[].class)
                        .setParameter("start", d1)
                        .setParameter("end", d2)
                        .getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        } else {
            String medecin[] = param.split(" ");
            System.err.println("koko param " + medecin[0] + " " + medecin[1]);

            query = "SELECT f.nom,f.prenom,count(*) "
                    + "FROM Operation o,Fonctionnaire f "
                    + "where f.id=o.medecinId "
                    + "and DATE(o.dateOperation)>=:start "
                    + "and DATE(o.dateOperation)<=:end "
                    + "and  f.nom=:nom"
                    + " and f.prenom=:prenom";

            try {

                listtNumberOfOperationByMedecin = sessionObj.createQuery(query, Object[].class)
                        .setParameter("nom", medecin[0])
                        .setParameter("prenom", medecin[1])
                        .setParameter("start", d1)
                        .setParameter("end", d2)
                        .getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        }

        return listtNumberOfOperationByMedecin;

    }

    public List<Object[]> getNumberOfOperationByAnnée() {
        List<Object[]> listtNumberOfOperationByAnnée = null;

        try {

            listtNumberOfOperationByAnnée = sessionObj.createQuery("Select year(o.dateOperation),count(*)"
                    + "from Operation o "
                    + "group by year(o.dateOperation)", Object[].class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listtNumberOfOperationByAnnée;

    }

    public List<Object[]> getNumberOfOperationByMonth() {

        List<Object[]> listtNumberOfOperationByMonth = null;

        try {

            listtNumberOfOperationByMonth = sessionObj.createQuery("Select year(o.dateOperation),MONTHNAME(o.dateOperation),count(*)"
                    + "from Operation o "
                    + "group by year(o.dateOperation),MONTHNAME(o.dateOperation)"
                    + "order by year(o.dateOperation),month(o.dateOperation)", Object[].class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listtNumberOfOperationByMonth;

    }

    public List<Object[]> getNumberOfOperationByAnnée(LocalDate start, LocalDate end) {
        List<Object[]> listtNumberOfOperationByAnnée = null;
        Date d1 = Date.from(start.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date d2 = Date.from(end.atStartOfDay(ZoneId.systemDefault()).toInstant());
        String query = "Select year(o.dateOperation),count(*)"
                + "from Operation o "
                + "where DATE(o.dateOperation)>=:start "
                + "and DATE(o.dateOperation)<=:end "
                + "group by year(o.dateOperation)";

        try {

            listtNumberOfOperationByAnnée = sessionObj.createQuery(query, Object[].class)
                    .setParameter("start", d1)
                    .setParameter("end", d2)
                    .getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listtNumberOfOperationByAnnée;

    }

    public List<Object[]> getNumberOfOperationByMonth(LocalDate start, LocalDate end) {

        List<Object[]> listtNumberOfOperationByMonth = null;
        Date d1 = Date.from(start.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date d2 = Date.from(end.atStartOfDay(ZoneId.systemDefault()).toInstant());
        String query = "Select year(o.dateOperation),MONTHNAME(o.dateOperation),count(*)"
                + "from Operation o "
                + "where DATE(o.dateOperation)>=:start "
                + "and DATE(o.dateOperation)<=:end "
                + "group by year(o.dateOperation),MONTHNAME(o.dateOperation)"
                + "order by year(o.dateOperation),month(o.dateOperation)";

        try {

            listtNumberOfOperationByMonth = sessionObj.createQuery(query, Object[].class)
                    .setParameter("start", d1)
                    .setParameter("end", d2)
                    .getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return listtNumberOfOperationByMonth;

    }

    public List<Acte> getActeList() {
        List<Acte> ActeList = null;

        try {

            ActeList = sessionObj.createQuery("Select a "
                    + "from Acte a", Acte.class).getResultList();

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
        }

        return ActeList;
    }

    public List<Object[]> getNumberOfOperationByActe(String param) {
        List<Object[]> listtNumberOfOperationByActe = null;
        String query = "";
        if (param.trim().toLowerCase().equals("tout")) {
            query = "Select a.description,count(*)"
                    + "from Operation o,Acte a,DetailOperation d "
                    + "where o.id=d.operationId "
                    + "and a.id=d.acteId "
                    + "group by a.description";
            try {

                listtNumberOfOperationByActe = sessionObj.createQuery(query, Object[].class).getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        } else {
            query = "Select a.description,count(*) "
                    + "from Operation o,Acte a,DetailOperation d "
                    + "where o.id=d.operationId "
                    + "and a.id=d.acteId "
                    + "and a.description=:description";

            try {

                listtNumberOfOperationByActe = sessionObj.createQuery(query, Object[].class).setParameter("description", param.trim()).getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }

        }

        return listtNumberOfOperationByActe;

    }

    public List<Object[]> getNumberOfOperationByActe(String param, LocalDate start, LocalDate end) {
        List<Object[]> listtNumberOfOperationByActe = null;
        Date d1 = Date.from(start.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date d2 = Date.from(end.atStartOfDay(ZoneId.systemDefault()).toInstant());
        String query = "";
        if (param.toLowerCase().trim().equals("tout")) {
            query = "Select a.description,count(*)"
                    + "from Operation o,Acte a,DetailOperation d "
                    + "where o.id=d.operationId "
                    + "and a.id=d.acteId "
                    + "and DATE(o.dateOperation)>=:start "
                    + "and DATE(o.dateOperation)<=:end "
                    + "group by a.description";
            try {

                listtNumberOfOperationByActe = sessionObj.createQuery(query, Object[].class)
                        .setParameter("start", d1)
                        .setParameter("end", d2)
                        .getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }
        } else {
            query = "Select a.description,count(*) "
                    + "from Operation o,Acte a,DetailOperation d "
                    + "where o.id=d.operationId "
                    + "and a.id=d.acteId "
                    + "and a.description=:description "
                    + "and DATE(o.dateOperation)>=:start "
                    + "and DATE(o.dateOperation)<=:end ";

            try {

                listtNumberOfOperationByActe = sessionObj.createQuery(query, Object[].class)
                        .setParameter("start", d1)
                        .setParameter("end", d2)
                        .setParameter("description", param.trim())
                        .getResultList();

            } catch (Exception sqlException) {

                sqlException.printStackTrace();
            }

        }

        return listtNumberOfOperationByActe;

    }

}
