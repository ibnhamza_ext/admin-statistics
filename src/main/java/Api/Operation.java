/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Api;

import Hibernate.OperationStat;
import Hibernate.PatientStat;
import JsonObjects.OperationByActe;
import JsonObjects.OperationByAnnée;
import JsonObjects.OperationByMedecin;
import JsonObjects.OperationByMonth;
import JsonObjects.PatientBySexe;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ROGUE1
 */
public class Operation extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json;charset=UTF-8");
        String param = "";
        if (request.getParameter("medecin") != null) {
            param = request.getParameter("medecin");
            OperationStat operation = new OperationStat();
            List<Object[]> listtNumberOfOperationByMedecin = null;

            if (request.getParameter("start") != null && request.getParameter("end") != null) {

                LocalDate start = LocalDate.parse(request.getParameter("start"));
                LocalDate end = LocalDate.parse(request.getParameter("end"));
                listtNumberOfOperationByMedecin = operation.getNumberOfOperationByMedecin(param, start, end);
                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByMedecin : listtNumberOfOperationByMedecin) {

                    OperationByMedecin operation_medecin = new OperationByMedecin((String) OperationByMedecin[0],
                            (String) OperationByMedecin[1],
                            new Integer(OperationByMedecin[2].toString()));
                    outputjson.add(operation_medecin);

                }

                String jsonOutPut = new Gson().toJson(outputjson);
                out.write(jsonOutPut);

            } else if (request.getParameter("start") != null && request.getParameter("end") == null) {
                LocalDate start = LocalDate.parse(request.getParameter("start"));
                listtNumberOfOperationByMedecin = operation.getNumberOfOperationByMedecin(param, start, LocalDate.now());
                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByMedecin : listtNumberOfOperationByMedecin) {

                    OperationByMedecin operation_medecin = new OperationByMedecin((String) OperationByMedecin[0],
                            (String) OperationByMedecin[1],
                            new Integer(OperationByMedecin[2].toString()));
                    outputjson.add(operation_medecin);

                }

                String jsonOutPut = new Gson().toJson(outputjson);
                out.write(jsonOutPut);

            } else if (request.getParameter("start") == null && request.getParameter("end") == null) {
                listtNumberOfOperationByMedecin = operation.getNumberOfOperationByMedecin(param);
                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByMedecin : listtNumberOfOperationByMedecin) {

                    OperationByMedecin operation_medecin = new OperationByMedecin((String) OperationByMedecin[0],
                            (String) OperationByMedecin[1],
                            new Integer(OperationByMedecin[2].toString()));
                    outputjson.add(operation_medecin);

                }

                String jsonOutPut = new Gson().toJson(outputjson);
                out.write(jsonOutPut);

            } else {
                JSONObject obj = new JSONObject();
                obj.put("error", "error in parameter");
                String jsonOutPut = new Gson().toJson(obj);
                out.write(jsonOutPut);

            }

        } else if (request.getParameter("acte") != null) {
            param = request.getParameter("acte");
            OperationStat operation = new OperationStat();
            List<Object[]> listtNumberOfOperationByActe = null;

            if (request.getParameter("start") != null && request.getParameter("end") != null) {
                LocalDate start = LocalDate.parse(request.getParameter("start"));
                LocalDate end = LocalDate.parse(request.getParameter("end"));
                listtNumberOfOperationByActe = operation.getNumberOfOperationByActe(param, start, end);

                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByActe : listtNumberOfOperationByActe) {

                    OperationByActe operation_acte = new OperationByActe((String) OperationByActe[0],
                            new Integer(OperationByActe[1].toString()));
                    outputjson.add(operation_acte);

                }
                String jsonOutPut = new Gson().toJson(outputjson);
                out.write(jsonOutPut);

            } else if (request.getParameter("start") != null && request.getParameter("end") == null) {

                LocalDate start = LocalDate.parse(request.getParameter("start"));

                listtNumberOfOperationByActe = operation.getNumberOfOperationByActe(param, start, LocalDate.now());

                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByActe : listtNumberOfOperationByActe) {

                    OperationByActe operation_acte = new OperationByActe((String) OperationByActe[0],
                            new Integer(OperationByActe[1].toString()));
                    outputjson.add(operation_acte);

                }
                String jsonOutPut = new Gson().toJson(outputjson);
                System.out.println("Api.Operation.doGet()"+jsonOutPut);
                out.write(jsonOutPut);

            } else if (request.getParameter("start") == null && request.getParameter("end") == null) {
                listtNumberOfOperationByActe = operation.getNumberOfOperationByActe(param);
                JSONArray outputjson = new JSONArray();
                for (Object[] OperationByActe : listtNumberOfOperationByActe) {

                    OperationByActe operation_acte = new OperationByActe((String) OperationByActe[0],
                            new Integer(OperationByActe[1].toString()));
                    outputjson.add(operation_acte);

                }
                String jsonOutPut = new Gson().toJson(outputjson);
                 System.out.println("Api.Operation.doGet()"+jsonOutPut);
                out.write(jsonOutPut);

            }

        } else if (request.getParameter("date") != null) {

            param = request.getParameter("date");
            OperationStat operation = new OperationStat();
            List<Object[]> listtNumberOfOperationByDate = null;

            if (request.getParameter("date").toString().toLowerCase().equals("year")) {
                if (request.getParameter("start") == null && request.getParameter("end") == null) {
                    listtNumberOfOperationByDate = operation.getNumberOfOperationByAnnée();
                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByAnnée operation_year = new OperationByAnnée(new Integer(OperationByDate[0].toString()),
                                new Integer(OperationByDate[1].toString()));
                        outputjson.add(operation_year);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                } else if (request.getParameter("start") != null && request.getParameter("end") != null) {
                    LocalDate start = LocalDate.parse(request.getParameter("start"));
                    LocalDate end = LocalDate.parse(request.getParameter("end"));
                    listtNumberOfOperationByDate = operation.getNumberOfOperationByAnnée(start, end);

                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByAnnée operation_year = new OperationByAnnée(new Integer(OperationByDate[0].toString()),
                                new Integer(OperationByDate[1].toString()));
                        outputjson.add(operation_year);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                } else if (request.getParameter("start") != null && request.getParameter("end") == null) {
                    LocalDate start = LocalDate.parse(request.getParameter("start"));

                    listtNumberOfOperationByDate = operation.getNumberOfOperationByAnnée(start, LocalDate.now());

                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByAnnée operation_year = new OperationByAnnée(new Integer(OperationByDate[0].toString()),
                                new Integer(OperationByDate[1].toString()));
                        outputjson.add(operation_year);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                }

            } else if (request.getParameter("date").toLowerCase().equals("mois")) {

                if (request.getParameter("start") == null && request.getParameter("end") == null) {
                    listtNumberOfOperationByDate = operation.getNumberOfOperationByMonth();
                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByMonth operation_mois = new OperationByMonth(new Integer(OperationByDate[0].toString()),
                                OperationByDate[1].toString(),
                                new Integer(OperationByDate[2].toString()));
                        outputjson.add(operation_mois);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                } else if (request.getParameter("start") != null && request.getParameter("end") != null) {
                    LocalDate start = LocalDate.parse(request.getParameter("start"));
                    LocalDate end = LocalDate.parse(request.getParameter("end"));
                    listtNumberOfOperationByDate = operation.getNumberOfOperationByMonth(start, end);

                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByMonth operation_mois = new OperationByMonth(new Integer(OperationByDate[0].toString()),
                                OperationByDate[1].toString(),
                                new Integer(OperationByDate[2].toString()));
                        outputjson.add(operation_mois);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                } else if (request.getParameter("start") != null && request.getParameter("end") == null) {
                    LocalDate start = LocalDate.parse(request.getParameter("start"));

                    listtNumberOfOperationByDate = operation.getNumberOfOperationByMonth(start, LocalDate.now());

                    JSONArray outputjson = new JSONArray();
                    for (Object[] OperationByDate : listtNumberOfOperationByDate) {

                        OperationByMonth operation_mois = new OperationByMonth(new Integer(OperationByDate[0].toString()),
                                OperationByDate[1].toString(),
                                new Integer(OperationByDate[2].toString()));
                        outputjson.add(operation_mois);

                    }

                    String jsonOutPut = new Gson().toJson(outputjson);
                    out.write(jsonOutPut);

                }

            }

        }

    }

}
