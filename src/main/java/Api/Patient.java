/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Api;

import Hibernate.HibernateUtil;
import Hibernate.PatientStat;
import JsonObjects.PatientByAgeRange;
import JsonObjects.PatientBySexe;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ROGUE1
 */
public class Patient extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json;charset=UTF-8");
        String param = "";
        if (request.getParameter("sexe") != null) {
            param = request.getParameter("sexe");
            PatientStat patient = new PatientStat();

            List<Object[]> listNumberOfPatientBySexe = patient.getNumberOfPatientBySexe(param);

            JSONArray outputjson = new JSONArray();
            for (Object[] patientSexe : listNumberOfPatientBySexe) {

                PatientBySexe patient_sexe = new PatientBySexe((String) patientSexe[0], new Integer(patientSexe[1].toString()));
                outputjson.add(patient_sexe);

            }

            String jsonOutPut = new Gson().toJson(outputjson);
            out.write(jsonOutPut);
        } else if (request.getParameter("age") != null) {
            param = request.getParameter("age");
            String param2 = request.getParameter("age_range");
            PatientStat patient = new PatientStat();
            List<Object[]> listNumberOfPatientByAge = null;

            System.out.println("Api.NumberOfPatientBySexe.doGet()" + param);
            if (param.toLowerCase().trim().equals("tout")) {
                System.out.println("Api.Patient.doGet() tout");
                listNumberOfPatientByAge = patient.getNumberOfPatientByAgeRange(param, param2);
            } else {
                listNumberOfPatientByAge = patient.getNumberOfPatientByAgeRangeApi(param);
            }

            JSONArray outputjson = new JSONArray();
            for (Object[] patientAge : listNumberOfPatientByAge) {
                // System.out.println("koko" + patientAge[0] + " " + patientAge[1]);
                if (patientAge[1] != null) {
                    PatientByAgeRange patient_age_range = new PatientByAgeRange(patientAge[1].toString(),
                            new Integer(patientAge[0].toString()));
                    outputjson.add(patient_age_range);

                }

            }
            String jsonOutPut = new Gson().toJson(outputjson);
            out.write(jsonOutPut);

        }

    }

}
