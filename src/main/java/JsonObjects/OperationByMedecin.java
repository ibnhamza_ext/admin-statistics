/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JsonObjects;

/**
 *
 * @author ROGUE1
 */
public class OperationByMedecin {
    
    private String nom;
    private String prénom;
    private int number_of_operation;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrénom() {
        return prénom;
    }

    public void setPrénom(String prénom) {
        this.prénom = prénom;
    }

    public int getNumber_of_operation() {
        return number_of_operation;
    }

    public void setNumber_of_operation(int number_of_operation) {
        this.number_of_operation = number_of_operation;
    }

    public OperationByMedecin(String nom, String prénom, int number_of_operation) {
        this.nom = nom;
        this.prénom = prénom;
        this.number_of_operation = number_of_operation;
    }

    @Override
    public String toString() {
        return "OperationByMedecin{" + "nom=" + nom + ", pr\u00e9nom=" + prénom + ", number_of_operation=" + number_of_operation + '}';
    }
    
    
    
    
    
    
}
