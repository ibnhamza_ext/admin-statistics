/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JsonObjects;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author ROGUE1
 */
public class PatientBySexe 

{
    
    private String Sexe;
  //  public  SimpleStringProperty Sexe_property;
    private int number_of_operation; 

    public PatientBySexe(String Sexe, int number_of_operation) {
        this.Sexe = Sexe;
        this.number_of_operation = number_of_operation;
    }

    public String getSexe() {
        return Sexe;
    }

    public int getNumber_of_operation() {
        return number_of_operation;
    }

    public void setSexe(String Sexe) {
        this.Sexe = Sexe;
    }

    public void setNumber_of_operation(int number_of_operation) {
        this.number_of_operation = number_of_operation;
    }
    
    
}
