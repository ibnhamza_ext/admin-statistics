/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JsonObjects;

/**
 *
 * @author ROGUE1
 */
public class PatientByAgeRange {
    
    private String ageRange;
    private int number_of_patient;

    public PatientByAgeRange(String ageRange, int number_of_patient) {
        this.ageRange = ageRange;
        this.number_of_patient = number_of_patient;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public int getNumber_of_patient() {
        return number_of_patient;
    }

    public void setNumber_of_patient(int number_of_patient) {
        this.number_of_patient = number_of_patient;
    }
    
    
    
    
    
}
