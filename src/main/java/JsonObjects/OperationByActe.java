/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JsonObjects;

/**
 *
 * @author ROGUE1
 */
public class OperationByActe {
    private String description;
    private int number_of_operation;

    public OperationByActe(String description, int number_of_operation) {
        this.description = description;
        this.number_of_operation = number_of_operation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumber_of_operation() {
        return number_of_operation;
    }

    public void setNumber_of_operation(int number_of_operation) {
        this.number_of_operation = number_of_operation;
    }
    
    
    
}
