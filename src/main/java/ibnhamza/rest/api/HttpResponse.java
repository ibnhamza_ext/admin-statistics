/**
 *
 * Copyright (C) IBn Hamza services & Micro Applications Eurl - All Rights Reserved
 *
 * This file is part of {project}
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * */
package ibnhamza.rest.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
public class HttpResponse {

    private HttpURLConnection httpConnection = null;
    private int responseCode = -1;
    private String response = null;
    
    public HttpResponse(HttpURLConnection http) {
        httpConnection = http;
    }

    public void read() throws IOException {
        responseCode = httpConnection.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpConnection.getInputStream()));
        String inputLine;
        StringBuilder stringBuffer = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            stringBuffer.append(inputLine);
        }
        response = stringBuffer.toString();
        in.close();
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}
