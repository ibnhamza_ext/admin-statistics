/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ibnhamza.rest.api;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;

/**
 *
 * @author BADEV
 */
public class HttpClient {

    private static String TOKEN = "";
    private final String USER_AGENT = "App RDV";
    public static String HOSTNAME = "localhost";
    public static String IP = "127.0.0.1";
    public static String PORT = "8099";

    // HTTP POST request
    public HttpResponse post(String url, String param) {
        try {
            URL obj = createURL(url);//new URL(baseURL.concat(url));
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("token", TOKEN);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
            writer.write(param);
            writer.flush();
            writer.close();
            wr.close();
            con.connect();
            System.out.print("koko");
            return new HttpResponse(con);

        } catch (IOException ex) {
            showError("Erreur de connexion!", "Verifiez que le serveur est en  cours d'exécution.");

            Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    // HTTP GET request
    public HttpResponse get(String url, String param) {
        try {
            URL obj = createURL(url.concat(param == null ? "" : "?"
                    + param.replaceAll("\\s+", "%20")));
            
         
           
           HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("GET");
            //add request header
            con.setRequestProperty("token", TOKEN);
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.connect();
          

            return new HttpResponse(con);
        

        } catch (IOException ex) {
            showError("Erreur de connexion!", "Verifiez que le serveur est en  cours d'exécution.");
            Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void setTOKEN(String TOKEN) {
        HttpClient.TOKEN = TOKEN;
    }

    public static void setIPAddress(String ip) {
        IP = ip;
    }

    public static void setHostname(String hostname) {
        HOSTNAME = hostname;
    }

    public static void setPort(String port) {
        PORT = port;
    }

    private URL createURL(String url) throws MalformedURLException {
        return new URL(String.format("http://%s:%s/api%s", IP, PORT, url));
    }

    public void showError(String title, String msg) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Attention!");
            alert.setHeaderText(title);
            alert.setContentText(msg);
            alert.showAndWait();

        });
    }

}
